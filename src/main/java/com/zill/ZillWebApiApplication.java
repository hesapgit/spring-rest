package com.zill;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZillWebApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZillWebApiApplication.class, args);
	}
}
