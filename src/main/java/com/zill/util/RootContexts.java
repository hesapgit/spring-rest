package com.zill.util;

public interface RootContexts {
	public static final String LANGUAGE_CODE = "langCode";
	public static final String RESTAURANT_ID = "restaurantId";
	public static final String REFERENCE_IMAGE_ID = "referenceId";
	public static final String MENU_ITEM_ID = "menuItemId";
	public static final String MENU_ID = "menuId";
	public static final String USER_ID = "userId";
	public static final String PROFILE_ID = "profıleId";

	public static final String SERVICE_COMMONS_PATH = "/commons";
	public static final String IMAGE_PATH = "/images";

	public static final String SERVER_IMAGE_PATH = "C:" + IMAGE_PATH;

	public static final String WEB_API_VERSION = "/v1/{" + LANGUAGE_CODE + "}";

	public static final String RESTAURANT_ROOT = WEB_API_VERSION + "/restaurants";
	public static final String RESTAURANT_PATH = RESTAURANT_ROOT + "/{" + RESTAURANT_ID + "}";

	public static final String WEB_API_VERSION_1 = "/v1";
	public static final String RESTAURANTS_PATH_NAME = "restaurants";

	public static final String MENUS_PATH_NAME = "menus";
	public static final String MENU_GROUPS_PATH_NAME = "menugroups";

	public static final String INGREDIENT_PATH_NAME = "ingredients";
	public static final String INGREDIENT_ROOT = WEB_API_VERSION_1 + "/" + INGREDIENT_PATH_NAME;

	public static final String RESTAURANT_GROUP_PATH_NAME = "restaurantcategories";
	public static final String RESTAURANT_GROUP_ROOT = WEB_API_VERSION_1 + "/" + RESTAURANT_GROUP_PATH_NAME;

	public static final String MENU_GROUP_PATH_NAME = "menugroups";
	public static final String MENU_GROUP_ROOT = WEB_API_VERSION_1 + "/" + MENU_GROUP_PATH_NAME;

	public static final String GENERIC_PATH = WEB_API_VERSION_1 + "/" + RESTAURANTS_PATH_NAME;

	public static final String MENU_ITEM_ROOT_PATH = GENERIC_PATH + "/{" + RESTAURANT_ID + "}/menus";
	public static final String MENU_ROOT_PATH = GENERIC_PATH + "/{" + RESTAURANT_ID + "}/restaurantmenu";

	public static final String IMAGE_ROOT_PATH = "/restaurant/image" + "/{" + RESTAURANT_ID + "}";

	public static final String USER_ROOT_PATH ="/user";
	public static final String USER_ID_ROOT_PATH = USER_ROOT_PATH + "/{" + USER_ID + "}";
	// public static final String MENU_GROUP_ROOT = WEB_API_VERSION_1 + "/" +
	// RESTAURANTS_PATH_NAME + "/{" + RESTAURANT_ID
	// + "}/menugroups";
}
