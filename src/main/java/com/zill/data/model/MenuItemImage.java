package com.zill.data.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The persistent class for the MENU_ITEM_IMAGES database table.
 * 
 */
@Entity
@Table(name = "MENU_ITEM_IMAGE")
@NamedQuery(name = "MenuItemImage.findAll", query = "SELECT m FROM MenuItemImage m")
public class MenuItemImage extends BaseModel {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	// bi-directional many-to-one association to MenuItem
	@ManyToOne
	@JoinColumn(name = "MENU_ITEM_ID")
	private MenuItem menuItem;
	@OneToOne
	@JoinColumn(name = "IMAGE_DETAIL_ID")
	private ImageDetail imageDetail;

	public MenuItemImage() {
	}

	public MenuItem getMenuItem() {
		return this.menuItem;
	}

	public void setMenuItem(MenuItem menuItem) {
		this.menuItem = menuItem;
	}

	public ImageDetail getImageDetail() {
		return imageDetail;
	}

	public void setImageDetail(ImageDetail imageDetail) {
		this.imageDetail = imageDetail;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
