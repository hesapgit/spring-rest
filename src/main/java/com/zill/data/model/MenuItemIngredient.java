package com.zill.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the MENU_ITEM_INGREDIENT database table.
 * 
 */
@Entity
@Table(name = "MENU_ITEM_INGREDIENT")
@NamedQuery(name = "MenuItemIngredient.findAll", query = "SELECT m FROM MenuItemIngredient m")
public class MenuItemIngredient extends BaseModel {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	// bi-directional many-to-one association to Ingredient
	@Column(name = "INGREDIENT_ID")
	private Long ingredientId;

	@Column(name = "MENU_ITEM_ID")
	private Long menuItemId;

	// @ManyToOne
	// @JoinColumn(name = "INGREDIENT_ID")
	// private Ingredient ingredient;

	// bi-directional many-to-one association to MenuItem
	// @ManyToOne
	// @JoinColumn(name="MENU_ITEM_ID")
	// private MenuItem menuItem;

	public MenuItemIngredient() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIngredientId() {
		return ingredientId;
	}

	public void setIngredientId(Long ingredientId) {
		this.ingredientId = ingredientId;
	}

	public Long getMenuItemId() {
		return menuItemId;
	}

	public void setMenuItemId(Long menuItemId) {
		this.menuItemId = menuItemId;
	}

	// public Ingredient getIngredient() {
	// return this.ingredient;
	// }
	//
	// public void setIngredient(Ingredient ingredient) {
	// this.ingredient = ingredient;
	// }
	//
	// public MenuItem getMenuItem() {
	// return this.menuItem;
	// }
	//
	// public void setMenuItem(MenuItem menuItem) {
	// this.menuItem = menuItem;
	// }

}