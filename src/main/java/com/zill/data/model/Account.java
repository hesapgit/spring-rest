package com.zill.data.model;


import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the Account database table.
 * 
 */
@Entity
@Table(name = "ACCOUNT")
@NamedQuery(name = "Account.findAll", query = "SELECT a FROM Account a")
public class Account extends BaseModel {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private boolean credentialsexpired;

	private boolean enabled;

	private boolean expired;

	private boolean locked;

	private String password;

	private String username;

	@Column(name = "CREATED_AT")
	private Timestamp createdAt;

	@Column(name = "EMAIL_CONFIRMATION_TOKEN")
	private String emailConfirmationToken;

	@Column(name = "PASSWORD_HASH")
	private String passwordHash;

	@Column(name = "PASSWORD_REMINDER_EXPIRE")
	private String passwordReminderExpire;

	@Column(name = "PASSWORD_REMINDER_TOKEN")
	private String passwordReminderToken;

	@Column(name = "PASWORD_SALT")
	private String paswordSalt;

	@ManyToOne
	@JoinColumn(name = "ACCOUNT_STATUS_ID")
	private AccountStatus accountStatus;

	// bi-directional many-to-one association to Profile
	@ManyToOne
	@JoinColumn(name = "PROFILE_ID")
	private Profile profile;

	// bi-directional many-to-many association to Role
	@ManyToMany(mappedBy = "accounts", fetch = FetchType.EAGER)
	private List<Role> roles;

	public Account() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean getCredentialsexpired() {
		return this.credentialsexpired;
	}

	public void setCredentialsexpired(boolean credentialsexpired) {
		this.credentialsexpired = credentialsexpired;
	}

	public boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean getExpired() {
		return this.expired;
	}

	public void setExpired(boolean expired) {
		this.expired = expired;
	}

	public boolean getLocked() {
		return this.locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getEmailConfirmationToken() {
		return emailConfirmationToken;
	}

	public void setEmailConfirmationToken(String emailConfirmationToken) {
		this.emailConfirmationToken = emailConfirmationToken;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public String getPasswordReminderExpire() {
		return passwordReminderExpire;
	}

	public void setPasswordReminderExpire(String passwordReminderExpire) {
		this.passwordReminderExpire = passwordReminderExpire;
	}

	public String getPasswordReminderToken() {
		return passwordReminderToken;
	}

	public void setPasswordReminderToken(String passwordReminderToken) {
		this.passwordReminderToken = passwordReminderToken;
	}

	public String getPaswordSalt() {
		return paswordSalt;
	}

	public void setPaswordSalt(String paswordSalt) {
		this.paswordSalt = paswordSalt;
	}

	public AccountStatus getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(AccountStatus accountStatus) {
		this.accountStatus = accountStatus;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

}