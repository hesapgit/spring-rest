package com.zill.data.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the RESTAURANT_CATEGORY database table.
 * 
 */
@Entity
@Table(name = "RESTAURANT_CATEGORY")
@NamedQuery(name = "RestaurantCategory.findAll", query = "SELECT r FROM RestaurantCategory r")
public class RestaurantCategory extends BaseModel {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String code;

	// bi-directional many-to-one association to Restaurant
	@OneToMany(mappedBy = "restaurantCategory", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Restaurant> restaurants;

	// bi-directional many-to-one association to RestaurantCategoryTranslation
	@OneToMany(mappedBy = "restaurantCategory", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<RestaurantCategoryTranslation> restaurantCategoryTranslations;

	public RestaurantCategory() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<Restaurant> getRestaurants() {
		return this.restaurants;
	}

	public void setRestaurants(List<Restaurant> restaurants) {
		this.restaurants = restaurants;
	}

	public Restaurant addRestaurant(Restaurant restaurant) {
		getRestaurants().add(restaurant);
		restaurant.setRestaurantCategory(this);

		return restaurant;
	}

	public Restaurant removeRestaurant(Restaurant restaurant) {
		getRestaurants().remove(restaurant);
		restaurant.setRestaurantCategory(null);

		return restaurant;
	}

	public List<RestaurantCategoryTranslation> getRestaurantCategoryTranslations() {
		return this.restaurantCategoryTranslations;
	}

	public void setRestaurantCategoryTranslations(List<RestaurantCategoryTranslation> restaurantCategoryTranslations) {
		this.restaurantCategoryTranslations = restaurantCategoryTranslations;
	}

	public RestaurantCategoryTranslation addRestaurantCategoryTranslation(
			RestaurantCategoryTranslation restaurantCategoryTranslation) {
		getRestaurantCategoryTranslations().add(restaurantCategoryTranslation);
		restaurantCategoryTranslation.setRestaurantCategory(this);

		return restaurantCategoryTranslation;
	}

	public RestaurantCategoryTranslation removeRestaurantCategoryTranslation(
			RestaurantCategoryTranslation restaurantCategoryTranslation) {
		getRestaurantCategoryTranslations().remove(restaurantCategoryTranslation);
		restaurantCategoryTranslation.setRestaurantCategory(null);

		return restaurantCategoryTranslation;
	}

}