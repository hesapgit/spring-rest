package com.zill.data.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the LANGUAGES database table.
 * 
 */
@Entity
@Table(name = "LANGUAGES")
@NamedQuery(name = "Language.findAll", query = "SELECT l FROM Language l")
public class Language extends BaseModel {
	private static final long serialVersionUID = 1L;

	@Id
	private String code;

	private String name;

	// bi-directional many-to-one association to MenuGroupTranslation
	@OneToMany(mappedBy = "language", fetch = FetchType.LAZY)
	private List<MenuGroupTranslation> menuGroupTranslations;

	// bi-directional many-to-one association to MenuItemTranslation
	@OneToMany(mappedBy = "language", fetch = FetchType.LAZY)
	private List<MenuItemTranslation> menuItemTranslations;

	// bi-directional many-to-one association to Restaurant
	@OneToMany(mappedBy = "language", fetch = FetchType.LAZY)
	private List<Restaurant> restaurants;

	// bi-directional many-to-one association to RestaurantCategoryTranslation
	@OneToMany(mappedBy = "language", fetch = FetchType.LAZY)
	private List<RestaurantCategoryTranslation> restaurantCategoryTranslations;

	// bi-directional many-to-one association to RestaurantTranslation
	@OneToMany(mappedBy = "language", fetch = FetchType.LAZY)
	private List<RestaurantTranslation> restaurantTranslations;

	// bi-directional many-to-one association to IngredientTranslation
	@OneToMany(mappedBy = "language", fetch = FetchType.LAZY)
	private List<IngredientTranslation> ingredientTranslations;

	// bi-directional many-to-one association to MenuTranslation
	@OneToMany(mappedBy = "language", fetch = FetchType.LAZY)
	private List<MenuTranslation> menuTranslations;

	public Language() {
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<IngredientTranslation> getIngredientTranslations() {
		return this.ingredientTranslations;
	}

	public void setIngredientTranslations(List<IngredientTranslation> ingredientTranslations) {
		this.ingredientTranslations = ingredientTranslations;
	}

	public IngredientTranslation addIngredientTranslation(IngredientTranslation ingredientTranslation) {
		getIngredientTranslations().add(ingredientTranslation);
		ingredientTranslation.setLanguage(this);

		return ingredientTranslation;
	}

	public IngredientTranslation removeIngredientTranslation(IngredientTranslation ingredientTranslation) {
		getIngredientTranslations().remove(ingredientTranslation);
		ingredientTranslation.setLanguage(null);

		return ingredientTranslation;
	}

	public List<MenuGroupTranslation> getMenuGroupTranslations() {
		return this.menuGroupTranslations;
	}

	public void setMenuGroupTranslations(List<MenuGroupTranslation> menuGroupTranslations) {
		this.menuGroupTranslations = menuGroupTranslations;
	}

	public MenuGroupTranslation addMenuGroupTranslation(MenuGroupTranslation menuGroupTranslation) {
		getMenuGroupTranslations().add(menuGroupTranslation);
		menuGroupTranslation.setLanguage(this);

		return menuGroupTranslation;
	}

	public MenuGroupTranslation removeMenuGroupTranslation(MenuGroupTranslation menuGroupTranslation) {
		getMenuGroupTranslations().remove(menuGroupTranslation);
		menuGroupTranslation.setLanguage(null);

		return menuGroupTranslation;
	}

	public List<MenuItemTranslation> getMenuItemTranslations() {
		return this.menuItemTranslations;
	}

	public void setMenuItemTranslations(List<MenuItemTranslation> menuItemTranslations) {
		this.menuItemTranslations = menuItemTranslations;
	}

	public MenuItemTranslation addMenuItemTranslation(MenuItemTranslation menuItemTranslation) {
		getMenuItemTranslations().add(menuItemTranslation);
		menuItemTranslation.setLanguage(this);

		return menuItemTranslation;
	}

	public MenuItemTranslation removeMenuItemTranslation(MenuItemTranslation menuItemTranslation) {
		getMenuItemTranslations().remove(menuItemTranslation);
		menuItemTranslation.setLanguage(null);

		return menuItemTranslation;
	}

	public List<MenuTranslation> getMenuTranslations() {
		return this.menuTranslations;
	}

	public void setMenuTranslations(List<MenuTranslation> menuTranslations) {
		this.menuTranslations = menuTranslations;
	}

	public MenuTranslation addMenuTranslation(MenuTranslation menuTranslation) {
		getMenuTranslations().add(menuTranslation);
		menuTranslation.setLanguage(this);

		return menuTranslation;
	}

	public MenuTranslation removeMenuTranslation(MenuTranslation menuTranslation) {
		getMenuTranslations().remove(menuTranslation);
		menuTranslation.setLanguage(null);

		return menuTranslation;
	}

	public List<Restaurant> getRestaurants() {
		return this.restaurants;
	}

	public void setRestaurants(List<Restaurant> restaurants) {
		this.restaurants = restaurants;
	}

	public Restaurant addRestaurant(Restaurant restaurant) {
		getRestaurants().add(restaurant);
		restaurant.setLanguage(this);

		return restaurant;
	}

	public Restaurant removeRestaurant(Restaurant restaurant) {
		getRestaurants().remove(restaurant);
		restaurant.setLanguage(null);

		return restaurant;
	}

	public List<RestaurantCategoryTranslation> getRestaurantCategoryTranslations() {
		return this.restaurantCategoryTranslations;
	}

	public void setRestaurantCategoryTranslations(List<RestaurantCategoryTranslation> restaurantCategoryTranslations) {
		this.restaurantCategoryTranslations = restaurantCategoryTranslations;
	}

	public RestaurantCategoryTranslation addRestaurantCategoryTranslation(
			RestaurantCategoryTranslation restaurantCategoryTranslation) {
		getRestaurantCategoryTranslations().add(restaurantCategoryTranslation);
		restaurantCategoryTranslation.setLanguage(this);

		return restaurantCategoryTranslation;
	}

	public RestaurantCategoryTranslation removeRestaurantCategoryTranslation(
			RestaurantCategoryTranslation restaurantCategoryTranslation) {
		getRestaurantCategoryTranslations().remove(restaurantCategoryTranslation);
		restaurantCategoryTranslation.setLanguage(null);

		return restaurantCategoryTranslation;
	}

	public List<RestaurantTranslation> getRestaurantTranslations() {
		return this.restaurantTranslations;
	}

	public void setRestaurantTranslations(List<RestaurantTranslation> restaurantTranslations) {
		this.restaurantTranslations = restaurantTranslations;
	}

	public RestaurantTranslation addRestaurantTranslation(RestaurantTranslation restaurantTranslation) {
		getRestaurantTranslations().add(restaurantTranslation);
		restaurantTranslation.setLanguage(this);

		return restaurantTranslation;
	}

	public RestaurantTranslation removeRestaurantTranslation(RestaurantTranslation restaurantTranslation) {
		getRestaurantTranslations().remove(restaurantTranslation);
		restaurantTranslation.setLanguage(null);

		return restaurantTranslation;
	}

}