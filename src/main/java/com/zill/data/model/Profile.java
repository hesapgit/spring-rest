package com.zill.data.model;


import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the PROFILE database table.
 * 
 */
@Entity
@Table(name = "PROFILE")
@NamedQuery(name = "Profile.findAll", query = "SELECT p FROM Profile p")
public class Profile extends BaseModel {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "CREATED_AT")
	private Timestamp createdAt;

	private String email;

	@Column(name = "FIRST_NAME")
	private String firstName;

	@Column(name = "FULL_NAME")
	private String fullName;

	@Column(name = "LAST_NAME")
	private String lastName;

	// bi-directional many-to-one association to Account
	@OneToMany(mappedBy = "profile", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Account> accounts;

	// bi-directional many-to-one association to AuthenticationProvider
	@OneToMany(mappedBy = "profile", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<AuthenticationProvider> authenticationProviders;

	public Profile() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<Account> getAccounts() {
		return this.accounts;
	}

	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}

	public Account addAccount(Account account) {
		getAccounts().add(account);
		account.setProfile(this);

		return account;
	}

	public Account removeAccount(Account account) {
		getAccounts().remove(account);
		account.setProfile(null);

		return account;
	}

	public List<AuthenticationProvider> getAuthenticationProviders() {
		return this.authenticationProviders;
	}

	public void setAuthenticationProviders(List<AuthenticationProvider> authenticationProviders) {
		this.authenticationProviders = authenticationProviders;
	}

	public AuthenticationProvider addAuthenticationProvider(AuthenticationProvider authenticationProvider) {
		getAuthenticationProviders().add(authenticationProvider);
		authenticationProvider.setProfile(this);

		return authenticationProvider;
	}

	public AuthenticationProvider removeAuthenticationProvider(AuthenticationProvider authenticationProvider) {
		getAuthenticationProviders().remove(authenticationProvider);
		authenticationProvider.setProfile(null);

		return authenticationProvider;
	}

}