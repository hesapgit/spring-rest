package com.zill.data.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the USER_MENU database table.
 * 
 */
@Entity
@Table(name = "USER_MENU")
@NamedQuery(name = "UserMenu.findAll", query = "SELECT u FROM UserMenu u")
public class UserMenu implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	// bi-directional many-to-one association to Profile
	@Column(name = "PROFILE_ID")
	private Long profileId;

	// bi-directional many-to-one association to Menu
	@Column(name = "MENU_ID")
	private Long menuId;

	public UserMenu() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProfile() {
		return this.profileId;
	}

	public void setProfile(Long profileId) {
		this.profileId = profileId;
	}

	public Long getMenu() {
		return this.menuId;
	}

	public void setMenu(Long menuId) {
		this.menuId = menuId;
	}

}