package com.zill.data.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the INGREDIENT_TRANSLATION database table.
 * 
 */
@Entity
@Table(name = "INGREDIENT_TRANSLATION")
@NamedQuery(name = "IngredientTranslation.findAll", query = "SELECT i FROM IngredientTranslation i")
public class IngredientTranslation extends BaseModel {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String name;

	// bi-directional many-to-one association to Ingredient
	@ManyToOne
	@JoinColumn(name = "INGREDIENT_ID")
	private Ingredient ingredient;

	// bi-directional many-to-one association to Language
	@ManyToOne
	@JoinColumn(name = "LANGUAGE_CODE")
	private Language language;

	public IngredientTranslation() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Ingredient getIngredient() {
		return this.ingredient;
	}

	public void setIngredient(Ingredient ingredient) {
		this.ingredient = ingredient;
	}

	public Language getLanguage() {
		return this.language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

}