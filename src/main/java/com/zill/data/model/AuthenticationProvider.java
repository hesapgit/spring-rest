package com.zill.data.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the AUTHENTICATION_PROVIDER database table.
 * 
 */
@Entity
@Table(name = "AUTHENTICATION_PROVIDER")
@NamedQuery(name = "AuthenticationProvider.findAll", query = "SELECT a FROM AuthenticationProvider a")
public class AuthenticationProvider implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "PROVIDER_KEY")
	private String providerKey;

	private String providerType;

	// bi-directional many-to-one association to Profile
	@ManyToOne
	@JoinColumn(name = "PROFILE_ID")
	private Profile profile;

	public AuthenticationProvider() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProviderKey() {
		return this.providerKey;
	}

	public void setProviderKey(String providerKey) {
		this.providerKey = providerKey;
	}

	public String getProviderType() {
		return this.providerType;
	}

	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

}