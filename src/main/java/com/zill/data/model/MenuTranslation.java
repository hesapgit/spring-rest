package com.zill.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the MENU_TRANSLATIONS database table.
 * 
 */
@Entity
@Table(name = "MENU_TRANSLATIONS")
@NamedQuery(name = "MenuTranslation.findAll", query = "SELECT m FROM MenuTranslation m")
public class MenuTranslation extends BaseModel {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String id;

	private String description;

	@Column(name = "TITLE")
	private String title;

	// bi-directional many-to-one association to Language
	@ManyToOne
	@JoinColumn(name = "LANGUAGES_CODE")
	private Language language;

	// bi-directional many-to-one association to Menu
	@ManyToOne
	@JoinColumn(name = "MENU_ID")
	private Menu menu;

	public MenuTranslation() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.title;
	}

	public void setName(String title) {
		this.title = title;
	}

	public Language getLanguage() {
		return this.language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public Menu getMenu() {
		return this.menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

}