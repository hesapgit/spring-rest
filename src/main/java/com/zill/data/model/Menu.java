package com.zill.data.model;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the MENU database table.
 * 
 */
@Entity
@Table(name = "MENU")
@NamedQuery(name = "Menu.findAll", query = "SELECT m FROM Menu m")
public class Menu extends BaseModel {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "GENERIC_NAME")
	private String genericName;

	@Column(name = "SPECIAL_OFFER")
	private double specialOffer;

	@Column(name = "CREATED_AT")
	private Timestamp createdAt;

	// bi-directional many-to-one association to ItemsInMenus
	// @OneToMany(mappedBy="menu", fetch = FetchType.EAGER, cascade =
	// CascadeType.ALL)
	// private List<ItemsInMenus> itemsInMenus;

	// bi-directional many-to-one association to Restaurant
	@ManyToOne
	@JoinColumn(name = "RESTAURANT_ID")
	private Restaurant restaurant;

	// bi-directional many-to-one association to MenuImage
	@OneToMany(mappedBy = "menu", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<MenuImage> menuImages;

	// bi-directional many-to-one association to MenuTranslation
	@OneToMany(mappedBy = "menu")
	private List<MenuTranslation> menuTranslations;

	@Column(name = "PARENT_MENU_ID")
	private Long parentMenuId;

	// bi-directional many-to-one association to Menu
	// @ManyToOne
	// @JoinColumn(name="PARENT_MENU_ID")
	// private Menu menu;

	// bi-directional many-to-one association to Menu
	// @OneToMany(mappedBy="menu")
	// private List<Menu> menus;

	public Menu() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGenericName() {
		return this.genericName;
	}

	public void setGenericName(String genericName) {
		this.genericName = genericName;
	}

	// public List<ItemsInMenus> getItemsInMenuses() {
	// return this.itemsInMenus;
	// }
	//
	// public void setItemsInMenuses(List<ItemsInMenus> itemsInMenus) {
	// this.itemsInMenus = itemsInMenus;
	// }
	//
	// public ItemsInMenus addItemsInMenus(ItemsInMenus itemsInMenus) {
	// getItemsInMenuses().add(itemsInMenus);
	// itemsInMenus.setMenu(this);
	//
	// return itemsInMenus;
	// }
	//
	// public ItemsInMenus removeItemsInMenus(ItemsInMenus itemsInMenus) {
	// getItemsInMenuses().remove(itemsInMenus);
	// itemsInMenus.setMenu(null);
	//
	// return itemsInMenus;
	// }

	public Restaurant getRestaurant() {
		return this.restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	public List<MenuImage> getMenuImages() {
		return this.menuImages;
	}

	public void setMenuImages(List<MenuImage> menuImages) {
		this.menuImages = menuImages;
	}

	public MenuImage addMenuImage(MenuImage menuImage) {
		getMenuImages().add(menuImage);
		menuImage.setMenu(this);

		return menuImage;
	}

	public MenuImage removeMenuImage(MenuImage menuImage) {
		getMenuImages().remove(menuImage);
		menuImage.setMenu(null);

		return menuImage;
	}

	public List<MenuTranslation> getMenuTranslations() {
		return this.menuTranslations;
	}

	public void setMenuTranslations(List<MenuTranslation> menuTranslations) {
		this.menuTranslations = menuTranslations;
	}

	public MenuTranslation addMenuTranslation(MenuTranslation menuTranslation) {
		getMenuTranslations().add(menuTranslation);
		menuTranslation.setMenu(this);

		return menuTranslation;
	}

	public MenuTranslation removeMenuTranslation(MenuTranslation menuTranslation) {
		getMenuTranslations().remove(menuTranslation);
		menuTranslation.setMenu(null);

		return menuTranslation;
	}

	// public Menu getMenu() {
	// return this.menu;
	// }
	//
	// public void setMenu(Menu menu) {
	// this.menu = menu;
	// }
	//
	// public List<Menu> getMenus() {
	// return this.menus;
	// }
	//
	// public void setMenus(List<Menu> menus) {
	// this.menus = menus;
	// }

	// public Menu addMenus(Menu menus) {
	// getMenus().add(menus);
	// menus.setMenu(this);
	//
	// return menus;
	// }
	// public Menu removeMenus(Menu menus) {
	// getMenus().remove(menus);
	// menus.setMenu(null);
	//
	// return menus;
	// }

	public Long getParentMenuId() {
		return parentMenuId;
	}

	public void setParentMenuId(Long parentMenuId) {
		this.parentMenuId = parentMenuId;
	}

	public double getSpecialOffer() {
		return this.specialOffer;
	}

	public void setSpecialOffer(double specialOffer) {
		this.specialOffer = specialOffer;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdDate) {
		this.createdAt = createdDate;
	}

}