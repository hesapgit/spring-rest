package com.zill.data.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the USER_RESTAURANT database table.
 * 
 */
@Entity
@Table(name = "USER_RESTAURANT")
@NamedQuery(name = "UserRestaurant.findAll", query = "SELECT u FROM UserRestaurant u")
public class UserRestaurant implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	// bi-directional many-to-one association to Profile
	@Column(name = "PROFILE_ID")
	private Long profileId;

	// bi-directional many-to-one association to Restaurant
	@Column(name = "RESTAURANT_ID")
	private Long restaurantId;

	public UserRestaurant() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProfile() {
		return this.profileId;
	}

	public void setProfile(Long profileId) {
		this.profileId = profileId;
	}

	public Long getRestaurant() {
		return this.restaurantId;
	}

	public void setRestaurant(Long restaurantId) {
		this.restaurantId = restaurantId;
	}

}