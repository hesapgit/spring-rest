package com.zill.data.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the MENU_GROUP database table.
 * 
 */
@Entity
@Table(name = "MENU_GROUP")
@NamedQuery(name = "MenuGroup.findAll", query = "SELECT m FROM MenuGroup m")
public class MenuGroup extends BaseModel {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String code;

	// bi-directional many-to-one association to MenuGroupTranslation
	@OneToMany(mappedBy = "menuGroup", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<MenuGroupTranslation> menuGroupTranslations;

	// bi-directional many-to-one association to MenuItem
	@OneToMany(mappedBy = "menuGroup", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<MenuItem> menuItems;

	public MenuGroup() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<MenuGroupTranslation> getMenuGroupTranslations() {
		return this.menuGroupTranslations;
	}

	public void setMenuGroupTranslations(List<MenuGroupTranslation> menuGroupTranslations) {
		this.menuGroupTranslations = menuGroupTranslations;
	}

	public MenuGroupTranslation addMenuGroupTranslation(MenuGroupTranslation menuGroupTranslation) {
		getMenuGroupTranslations().add(menuGroupTranslation);
		menuGroupTranslation.setMenuGroup(this);

		return menuGroupTranslation;
	}

	public MenuGroupTranslation removeMenuGroupTranslation(MenuGroupTranslation menuGroupTranslation) {
		getMenuGroupTranslations().remove(menuGroupTranslation);
		menuGroupTranslation.setMenuGroup(null);

		return menuGroupTranslation;
	}

	public List<MenuItem> getMenuItems() {
		return this.menuItems;
	}

	public void setMenuItems(List<MenuItem> menuItems) {
		this.menuItems = menuItems;
	}

	public MenuItem addMenuItem(MenuItem menuItem) {
		getMenuItems().add(menuItem);
		menuItem.setMenuGroup(this);

		return menuItem;
	}

	public MenuItem removeMenuItem(MenuItem menuItem) {
		getMenuItems().remove(menuItem);
		menuItem.setMenuGroup(null);

		return menuItem;
	}

}