package com.zill.data.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The persistent class for the IMAGE_DETAIL database table.
 * 
 */
@Entity
@Table(name = "IMAGE_DETAIL")
@NamedQuery(name = "ImageDetail.findAll", query = "SELECT i FROM ImageDetail i")
public class ImageDetail extends BaseModel {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String description;

	@Column(name = "IMAGE_PATH")
	private String imagePath;

	@Column(name = "REFERENCE_IMAGE_PATH")
	private String referenceImagePath = UUID.randomUUID().toString().replaceAll("-", "");

	private Integer size;

	// bi-directional many-to-one association to MenuImage
	@OneToOne(mappedBy = "imageDetail")
	private MenuImage menuImage;

	// bi-directional many-to-one association to MenuItemImage
	@OneToOne(mappedBy = "imageDetail")
	private MenuItemImage menuItemImage;

	// bi-directional many-to-one association to RestaurantImage
	@OneToOne(mappedBy = "imageDetail")
	private RestaurantImage restaurantImage;

	public ImageDetail() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImagePath() {
		return this.imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getReferenceImagePath() {
		return this.referenceImagePath;
	}

	public void setReferenceImagePath(String referenceImagePath) {
		this.referenceImagePath = referenceImagePath;
	}

	public Integer getSize() {
		return this.size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public MenuImage getMenuImage() {
		return menuImage;
	}

	public void setMenuImage(MenuImage menuImage) {
		this.menuImage = menuImage;
	}

	public MenuItemImage getMenuItemImage() {
		return menuItemImage;
	}

	public void setMenuItemImage(MenuItemImage menuItemImage) {
		this.menuItemImage = menuItemImage;
	}

	public RestaurantImage getRestaurantImage() {
		return restaurantImage;
	}

	public void setRestaurantImage(RestaurantImage restaurantImage) {
		this.restaurantImage = restaurantImage;
	}

}