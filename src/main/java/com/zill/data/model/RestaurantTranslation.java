package com.zill.data.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the RESTAURANT_TRANSLATION database table.
 * 
 */
@Entity
@Table(name = "RESTAURANT_TRANSLATION")
@NamedQuery(name = "RestaurantTranslation.findAll", query = "SELECT r FROM RestaurantTranslation r")
public class RestaurantTranslation extends BaseModel {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String information;

	// bi-directional many-to-one association to Language
	@ManyToOne
	@JoinColumn(name = "LANGUAGE_CODE")
	private Language language;

	// bi-directional many-to-one association to Restaurant
	@ManyToOne
	@JoinColumn(name = "RESTAURANT_ID")
	private Restaurant restaurant;

	public RestaurantTranslation() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getInformation() {
		return this.information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	public Language getLanguage() {
		return this.language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public Restaurant getRestaurant() {
		return this.restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

}