package com.zill.data.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The persistent class for the MENU_IMAGES database table.
 * 
 */
@Entity
@Table(name = "MENU_IMAGE")
@NamedQuery(name = "MenuImage.findAll", query = "SELECT m FROM MenuImage m")
public class MenuImage extends BaseModel {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	// bi-directional many-to-one association to Menu
	@ManyToOne
	@JoinColumn(name = "MENU_ID")
	private Menu menu;
	@OneToOne
	@JoinColumn(name = "IMAGE_DETAIL_ID")
	private ImageDetail imageDetail;

	public MenuImage() {
	}

	public Menu getMenu() {
		return this.menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	public ImageDetail getImageDetail() {
		return imageDetail;
	}

	public void setImageDetail(ImageDetail imageDetail) {
		this.imageDetail = imageDetail;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}