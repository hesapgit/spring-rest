package com.zill.data.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The persistent class for the RESTAURANT_IMAGES database table.
 * 
 */
@Entity
@Table(name = "RESTAURANT_IMAGE")
@NamedQuery(name = "RestaurantImage.findAll", query = "SELECT r FROM RestaurantImage r")
public class RestaurantImage extends BaseModel {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	// bi-directional many-to-one association to Restaurant
	@ManyToOne
	@JoinColumn(name = "RESTAURANT_ID")
	private Restaurant restaurant;

	@OneToOne
	@JoinColumn(name = "IMAGE_DETAIL_ID")
	private ImageDetail imageDetail;

	public RestaurantImage() {
	}

	public Restaurant getRestaurant() {
		return this.restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	public ImageDetail getImageDetail() {
		return imageDetail;
	}

	public void setImageDetail(ImageDetail imageDetail) {
		this.imageDetail = imageDetail;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}