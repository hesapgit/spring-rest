package com.zill.data.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the RESTAURANT_CATEGORY_TRANSLATION database table.
 * 
 */
@Entity
@Table(name = "RESTAURANT_CATEGORY_TRANSLATION")
@NamedQuery(name = "RestaurantCategoryTranslation.findAll", query = "SELECT r FROM RestaurantCategoryTranslation r")
public class RestaurantCategoryTranslation extends BaseModel {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String description;

	private String name;

	// bi-directional many-to-one association to Language
	@ManyToOne
	@JoinColumn(name = "LANGUAGE")
	private Language language;

	// bi-directional many-to-one association to RestaurantCategory
	@ManyToOne
	@JoinColumn(name = "RESTAURANT_CATEGORY_ID")
	private RestaurantCategory restaurantCategory;

	public RestaurantCategoryTranslation() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Language getLanguage() {
		return this.language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public RestaurantCategory getRestaurantCategory() {
		return this.restaurantCategory;
	}

	public void setRestaurantCategory(RestaurantCategory restaurantCategory) {
		this.restaurantCategory = restaurantCategory;
	}

}