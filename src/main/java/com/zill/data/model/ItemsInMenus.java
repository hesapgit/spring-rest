package com.zill.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the ITEMS_IN_MENUS database table.
 * 
 */
@Entity
@Table(name = "ITEMS_IN_MENUS")
@NamedQuery(name = "ItemsInMenus.findAll", query = "SELECT i FROM ItemsInMenus i")
public class ItemsInMenus extends BaseModel {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	// bi-directional many-to-one association to Menu

	@Column(name = "MENU_ID")
	private Long menuId;

	// bi-directional many-to-one association to MenuItem

	@Column(name = "MENU_ITEM_ID")
	private Long menuItemId;

	@Column(name = "ITEM_DISCOUNT_RATE")
	private int itemDiscountRate;

	@Column(name = "ITEM_PRICE")
	private double itemPrice;

	public ItemsInMenus() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getMenuId() {
		return this.menuId;
	}

	public void setMenuId(Long menuId) {
		this.menuId = menuId;
	}

	public Long getMenuItemId() {
		return this.menuItemId;
	}

	public void setMenuItemId(Long menuItemId) {
		this.menuItemId = menuItemId;
	}

	public int getItemDiscountRate() {
		return itemDiscountRate;
	}

	public void setItemDiscountRate(int itemDiscountRate) {
		this.itemDiscountRate = itemDiscountRate;
	}

	public double getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}

}