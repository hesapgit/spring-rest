package com.zill.data.model;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the RESTAURANT database table.
 * 
 */
@Entity
@Table(name = "RESTAURANT")
@NamedQuery(name = "Restaurant.findAll", query = "SELECT r FROM Restaurant r")
public class Restaurant extends BaseModel {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String name;

	// bi-directional many-to-one association to Menu
	@OneToMany(mappedBy = "restaurant")
	private List<Menu> menus;

	// bi-directional many-to-one association to MenuItem
	@OneToMany(mappedBy = "restaurant")
	private List<MenuItem> menuItems;

	// bi-directional many-to-one association to Language
	@ManyToOne
	@JoinColumn(name = "DEFAULT_LANG")
	private Language language;

	// bi-directional many-to-one association to RestaurantImage
	@OneToMany(mappedBy = "restaurant", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<RestaurantImage> restaurantImages;

	// bi-directional many-to-one association to RestaurantTranslation
	@OneToMany(mappedBy = "restaurant", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<RestaurantTranslation> restaurantTranslations;

	@ManyToOne
	@JoinColumn(name = "RESTAURANT_CATEGORY_ID")
	private RestaurantCategory restaurantCategory;

	@Column(name = "CREATED_At")
	private Timestamp createdAt;

	public Restaurant() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Menu> getMenus() {
		return this.menus;
	}

	public void setMenus(List<Menu> menus) {
		this.menus = menus;
	}

	public Menu addMenus(Menu menus) {
		getMenus().add(menus);
		menus.setRestaurant(this);

		return menus;
	}

	public Menu removeMenus(Menu menus) {
		getMenus().remove(menus);
		menus.setRestaurant(null);

		return menus;
	}

	public List<MenuItem> getMenuItems() {
		return this.menuItems;
	}

	public void setMenuItems(List<MenuItem> menuItems) {
		this.menuItems = menuItems;
	}

	public MenuItem addMenuItem(MenuItem menuItem) {
		getMenuItems().add(menuItem);
		menuItem.setRestaurant(this);

		return menuItem;
	}

	public MenuItem removeMenuItem(MenuItem menuItem) {
		getMenuItems().remove(menuItem);
		menuItem.setRestaurant(null);

		return menuItem;
	}

	public Language getLanguage() {
		return this.language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public List<RestaurantImage> getRestaurantImages() {
		return this.restaurantImages;
	}

	public void setRestaurantImages(List<RestaurantImage> restaurantImages) {
		this.restaurantImages = restaurantImages;
	}

	public RestaurantImage addRestaurantImage(RestaurantImage restaurantImage) {
		getRestaurantImages().add(restaurantImage);
		restaurantImage.setRestaurant(this);

		return restaurantImage;
	}

	public RestaurantImage removeRestaurantImage(RestaurantImage restaurantImage) {
		getRestaurantImages().remove(restaurantImage);
		restaurantImage.setRestaurant(null);

		return restaurantImage;
	}

	public List<RestaurantTranslation> getRestaurantTranslations() {
		return this.restaurantTranslations;
	}

	public void setRestaurantTranslations(List<RestaurantTranslation> restaurantTranslations) {
		this.restaurantTranslations = restaurantTranslations;
	}

	public RestaurantTranslation addRestaurantTranslation(RestaurantTranslation restaurantTranslation) {
		getRestaurantTranslations().add(restaurantTranslation);
		restaurantTranslation.setRestaurant(this);

		return restaurantTranslation;
	}

	public RestaurantTranslation removeRestaurantTranslation(RestaurantTranslation restaurantTranslation) {
		getRestaurantTranslations().remove(restaurantTranslation);
		restaurantTranslation.setRestaurant(null);

		return restaurantTranslation;
	}

	public RestaurantCategory getRestaurantCategory() {
		return restaurantCategory;
	}

	public void setRestaurantCategory(RestaurantCategory restaurantCategory) {
		this.restaurantCategory = restaurantCategory;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdDate) {
		this.createdAt = createdDate;
	}

}