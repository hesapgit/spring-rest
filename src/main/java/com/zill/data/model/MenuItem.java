package com.zill.data.model;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the MENU_ITEM database table.
 * 
 */
@Entity
@Table(name = "MENU_ITEM")
@NamedQuery(name = "MenuItem.findAll", query = "SELECT m FROM MenuItem m")
public class MenuItem extends BaseModel {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private int calori;

	@Column(name = "IS_HELAL")
	private boolean isHelal;

	@Column(name = "LIST_ORDER")
	private int listOrder;

	private String name;

	private double price;

	@Column(name = "SERVICE_TIME")
	private int serviceTime;

	// bi-directional many-to-one association to ItemsInMenus
	// @OneToMany(mappedBy="menuItem")
	// private List<ItemsInMenus> itemsInMenus;

	// bi-directional many-to-one association to Currency
	@ManyToOne
	@JoinColumn(name = "CURRENCY")
	private Currency currencyBean;

	// bi-directional many-to-one association to MenuGroup
	@ManyToOne
	@JoinColumn(name = "GROUP_ID")
	private MenuGroup menuGroup;

	// bi-directional many-to-one association to Restaurant
	@ManyToOne
	@JoinColumn(name = "RESTAURANT_ID")
	private Restaurant restaurant;

	// bi-directional many-to-one association to MenuItemImage
	@OneToMany(mappedBy = "menuItem", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<MenuItemImage> menuItemImages;

	// bi-directional many-to-one association to MenuItemIngredient
	// @OneToMany(mappedBy = "menuItem", fetch = FetchType.EAGER, cascade =
	// CascadeType.ALL)
	// private List<MenuItemIngredient> menuItemIngredients;

	// bi-directional many-to-one association to MenuItemTranslation
	@OneToMany(mappedBy = "menuItem", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<MenuItemTranslation> menuItemTranslations;

	@Column(name = "CREATED_AT")
	private Timestamp createdAt;

	public MenuItem() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getCalori() {
		return this.calori;
	}

	public void setCalori(int calori) {
		this.calori = calori;
	}

	public boolean getIsHelal() {
		return this.isHelal;
	}

	public void setIsHelal(boolean isHelal) {
		this.isHelal = isHelal;
	}

	public int getListOrder() {
		return this.listOrder;
	}

	public void setListOrder(int listOrder) {
		this.listOrder = listOrder;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return this.price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getServiceTime() {
		return this.serviceTime;
	}

	public void setServiceTime(int serviceTime) {
		this.serviceTime = serviceTime;
	}

	// public List<ItemsInMenus> getItemsInMenuses() {
	// return this.itemsInMenus;
	// }
	//
	// public void setItemsInMenuses(List<ItemsInMenus> itemsInMenus) {
	// this.itemsInMenus = itemsInMenus;
	// }

	// public ItemsInMenus addItemsInMenus(ItemsInMenus itemsInMenus) {
	// getItemsInMenuses().add(itemsInMenus);
	// itemsInMenus.setMenuItem(this);
	//
	// return itemsInMenus;
	// }
	//
	// public ItemsInMenus removeItemsInMenus(ItemsInMenus itemsInMenus) {
	// getItemsInMenuses().remove(itemsInMenus);
	// itemsInMenus.setMenuItem(null);
	//
	// return itemsInMenus;
	// }

	public Currency getCurrencyBean() {
		return this.currencyBean;
	}

	public void setCurrencyBean(Currency currencyBean) {
		this.currencyBean = currencyBean;
	}

	public MenuGroup getMenuGroup() {
		return this.menuGroup;
	}

	public void setMenuGroup(MenuGroup menuGroup) {
		this.menuGroup = menuGroup;
	}

	public Restaurant getRestaurant() {
		return this.restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	public List<MenuItemImage> getMenuItemImages() {
		return this.menuItemImages;
	}

	public void setMenuItemImages(List<MenuItemImage> menuItemImages) {
		this.menuItemImages = menuItemImages;
	}

	public MenuItemImage addMenuItemImage(MenuItemImage menuItemImage) {
		getMenuItemImages().add(menuItemImage);
		menuItemImage.setMenuItem(this);

		return menuItemImage;
	}

	public MenuItemImage removeMenuItemImage(MenuItemImage menuItemImage) {
		getMenuItemImages().remove(menuItemImage);
		menuItemImage.setMenuItem(null);

		return menuItemImage;
	}

	// public List<MenuItemIngredient> getMenuItemIngredients() {
	// return this.menuItemIngredients;
	// }
	//
	// public void setMenuItemIngredients(List<MenuItemIngredient>
	// menuItemIngredients) {
	// this.menuItemIngredients = menuItemIngredients;
	// }

	// public MenuItemIngredient addMenuItemIngredient(MenuItemIngredient
	// menuItemIngredient) {
	// getMenuItemIngredients().add(menuItemIngredient);
	// menuItemIngredient.setMenuItemId(this.id);
	//
	// return menuItemIngredient;
	// }
	//
	// public MenuItemIngredient removeMenuItemIngredient(MenuItemIngredient
	// menuItemIngredient) {
	// getMenuItemIngredients().remove(menuItemIngredient);
	// menuItemIngredient.setMenuItemId(null);
	//
	// return menuItemIngredient;
	// }

	public List<MenuItemTranslation> getMenuItemTranslations() {
		return this.menuItemTranslations;
	}

	public void setMenuItemTranslations(List<MenuItemTranslation> menuItemTranslations) {
		this.menuItemTranslations = menuItemTranslations;
	}

	public MenuItemTranslation addMenuItemTranslation(MenuItemTranslation menuItemTranslation) {
		getMenuItemTranslations().add(menuItemTranslation);
		menuItemTranslation.setMenuItem(this);

		return menuItemTranslation;
	}

	public MenuItemTranslation removeMenuItemTranslation(MenuItemTranslation menuItemTranslation) {
		getMenuItemTranslations().remove(menuItemTranslation);
		menuItemTranslation.setMenuItem(null);

		return menuItemTranslation;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdDate) {
		this.createdAt = createdDate;
	}

	public void setHelal(boolean isHelal) {
		this.isHelal = isHelal;
	}

}