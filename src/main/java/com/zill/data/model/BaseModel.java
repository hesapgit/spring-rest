package com.zill.data.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class BaseModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "UPDATED_AT")
	private Timestamp updateAT;

	public Timestamp getUpdatedAt() {
		return this.updateAT;
	}

	public void setUpdatedAt(Timestamp updateAT) {
		this.updateAT = updateAT;
	}

}
