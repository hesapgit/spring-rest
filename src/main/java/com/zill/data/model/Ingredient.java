package com.zill.data.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the INGREDIENT database table.
 * 
 */
@Entity
@Table(name = "INGREDIENT")
@NamedQuery(name = "Ingredient.findAll", query = "SELECT i FROM Ingredient i")
public class Ingredient implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "GENERIC_NAME")
	private String genericName;

	// bi-directional many-to-one association to IngredientTranslation
	@OneToMany(mappedBy = "ingredient", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<IngredientTranslation> ingredientTranslations;

	private Timestamp lastModifiedDate;

	// silinecek
	// bi-directional many-to-one association to MenuItemIngredient
	// @OneToMany(mappedBy="ingredient")
	// private List<MenuItemIngredient> menuItemIngredients;

	public Ingredient() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGenericName() {
		return this.genericName;
	}

	public void setGenericName(String genericName) {
		this.genericName = genericName;
	}

	public List<IngredientTranslation> getIngredientTranslations() {
		return this.ingredientTranslations;
	}

	public void setIngredientTranslations(List<IngredientTranslation> ingredientTranslations) {
		this.ingredientTranslations = ingredientTranslations;
	}

	public IngredientTranslation addIngredientTranslation(IngredientTranslation ingredientTranslation) {
		getIngredientTranslations().add(ingredientTranslation);
		ingredientTranslation.setIngredient(this);

		return ingredientTranslation;
	}

	public IngredientTranslation removeIngredientTranslation(IngredientTranslation ingredientTranslation) {
		getIngredientTranslations().remove(ingredientTranslation);
		ingredientTranslation.setIngredient(null);

		return ingredientTranslation;
	}

	public Timestamp getLastModifiedDate() {
		return this.lastModifiedDate;
	}

	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	// public List<MenuItemIngredient> getMenuItemIngredients() {
	// return this.menuItemIngredients;
	// }
	//
	// public void setMenuItemIngredients(List<MenuItemIngredient>
	// menuItemIngredients) {
	// this.menuItemIngredients = menuItemIngredients;
	// }

	// public MenuItemIngredient addMenuItemIngredient(MenuItemIngredient
	// menuItemIngredient) {
	// getMenuItemIngredients().add(menuItemIngredient);
	// menuItemIngredient.setIngredientId(this.id);
	//
	// return menuItemIngredient;
	// }
	//
	// public MenuItemIngredient removeMenuItemIngredient(MenuItemIngredient
	// menuItemIngredient) {
	// getMenuItemIngredients().remove(menuItemIngredient);
	// menuItemIngredient.setIngredientId(null);
	//
	// return menuItemIngredient;
	// }

}