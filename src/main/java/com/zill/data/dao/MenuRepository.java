package com.zill.data.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zill.data.model.Menu;

@Repository
public interface MenuRepository extends JpaRepository<Menu, Long> {

	public List<Menu> findByRestaurantId(Long restaurantId);

	// @Query("SELECT DISTINCT menu FROM Menu m WHERE m.restaurantId =
	// :restaurant")
	// public List<Menu> findDistinctByResturantId(@Param("restaurant")
	// Restaurant restaurantId);

}
