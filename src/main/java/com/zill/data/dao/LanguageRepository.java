package com.zill.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zill.data.model.Language;

@Repository
public interface LanguageRepository extends JpaRepository<Language, String> {

	public Language findByCode(String code);
}
