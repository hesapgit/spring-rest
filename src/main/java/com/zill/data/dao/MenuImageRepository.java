package com.zill.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zill.data.model.ImageDetail;
import com.zill.data.model.MenuImage;

@Repository
public interface MenuImageRepository extends JpaRepository<MenuImage, Long> {

	MenuImage findByImageDetail(ImageDetail requestImage);

}
