package com.zill.data.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zill.data.model.MenuItemIngredient;

@Repository
public interface MenuItemIngredientRepository extends JpaRepository<MenuItemIngredient, Long> {

	public List<MenuItemIngredient> findIngredientsByMenuItemId(Long menuItemId);
}
