package com.zill.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zill.data.model.AccountStatus;

@Repository
public interface AccountStatusRepository extends JpaRepository<AccountStatus, Long> {

	

}
