package com.zill.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zill.data.model.IngredientTranslation;

@Repository
public interface IngredientTranslationRepository extends JpaRepository<IngredientTranslation, Long> {
	public IngredientTranslation findOneByIngredientIdAndLanguageCode(Long IngredientId, String languageCode);
}
