package com.zill.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zill.data.model.Currency;

@Repository
public interface CurrencyRepository extends JpaRepository<Currency, String> {

	public Currency findByCode(String code);
}
