package com.zill.data.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.zill.data.model.MenuGroup;
import com.zill.data.model.MenuItem;
import com.zill.data.model.Restaurant;

@Repository
public interface MenuItemRepository extends JpaRepository<MenuItem, Long> {

	public List<MenuItem> findByRestaurantId(Long restaurantId);

	@Query("SELECT DISTINCT menuGroup FROM MenuItem m WHERE m.restaurant = :restaurant")
	public List<MenuGroup> findDistinctByRestaurantId(@Param("restaurant") Restaurant restaurantId);
}
