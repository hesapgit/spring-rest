package com.zill.data.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zill.data.model.UserRestaurant;

@Repository
public interface UserRestaurantRepository extends JpaRepository<UserRestaurant, Long> {

	List<UserRestaurant> findByProfileId(Long id);

}
