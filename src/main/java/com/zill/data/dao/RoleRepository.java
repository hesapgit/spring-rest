package com.zill.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zill.data.model.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

}
