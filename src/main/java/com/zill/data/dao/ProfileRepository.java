package com.zill.data.dao;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zill.data.model.Profile;

@Repository
public interface ProfileRepository extends JpaRepository<Profile, Long> {



}
