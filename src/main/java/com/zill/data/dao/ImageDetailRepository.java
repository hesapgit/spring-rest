package com.zill.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zill.data.model.ImageDetail;

@Repository
public interface ImageDetailRepository extends JpaRepository<ImageDetail, Long> {
	public ImageDetail findByReferenceImagePath(String referenceImagePath);
}
