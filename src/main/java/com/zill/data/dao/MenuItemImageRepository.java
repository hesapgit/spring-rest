package com.zill.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zill.data.model.ImageDetail;
import com.zill.data.model.MenuItemImage;

@Repository
public interface MenuItemImageRepository extends JpaRepository<MenuItemImage, Long> {

	MenuItemImage findByImageDetail(ImageDetail requestImage);

}
