package com.zill.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zill.data.model.AuthenticationProvider;

@Repository
public interface AuthenticationProviderRepository extends JpaRepository<AuthenticationProvider, Long> {

}
