package com.zill.data.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zill.data.model.ItemsInMenus;

@Repository
public interface ItemsInMenusRepository extends JpaRepository<ItemsInMenus, Long> {

	public List<ItemsInMenus> findMenuItemsByMenuId(Long menuId);
}