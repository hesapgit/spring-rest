package com.zill.data.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zill.data.model.UserMenu;

@Repository
public interface UserMenuRepository extends JpaRepository<UserMenu, Long> {

	public List<UserMenu> findByProfileId(Long id);


}
