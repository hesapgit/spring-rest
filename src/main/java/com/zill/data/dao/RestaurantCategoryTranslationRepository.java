package com.zill.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zill.data.model.RestaurantCategoryTranslation;

@Repository
public interface RestaurantCategoryTranslationRepository extends JpaRepository<RestaurantCategoryTranslation, Long> {
	public RestaurantCategoryTranslation findOneByRestaurantCategoryIdAndLanguageCode(Long ResturantId,
			String languageCode);

}
