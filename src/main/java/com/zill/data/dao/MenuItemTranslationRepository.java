package com.zill.data.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zill.data.model.MenuItemTranslation;

@Repository
public interface MenuItemTranslationRepository extends JpaRepository<MenuItemTranslation, Long> {

	public List<MenuItemTranslation> findByMenuItemId(Long menuItemId);
}
