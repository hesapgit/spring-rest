package com.zill.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zill.data.model.ImageDetail;
import com.zill.data.model.RestaurantImage;

@Repository
public interface RestaurantImageRepository extends JpaRepository<RestaurantImage, Long> {

	RestaurantImage findByImageDetail(ImageDetail requestImage);

}
