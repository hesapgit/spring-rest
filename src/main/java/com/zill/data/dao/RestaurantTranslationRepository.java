package com.zill.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zill.data.model.RestaurantTranslation;

@Repository
public interface RestaurantTranslationRepository extends JpaRepository<RestaurantTranslation, Long> {
	public RestaurantTranslation findOneByRestaurantIdAndLanguageCode(Long restaurantId, String languageCode);
}
