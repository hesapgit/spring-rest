package com.zill.service.model;

public class SMMenuLang extends SMDate {

	private String description;

	private String title;

	private String langCode;

	/**
	 * 
	 */
	public SMMenuLang() {
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLangCode() {
		return langCode;
	}

	public void setLangCode(String langCode) {
		this.langCode = langCode;
	}

}
