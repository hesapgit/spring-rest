package com.zill.service.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.deser.std.DateDeserializers.TimestampDeserializer;

public class SMDate {

	@JsonDeserialize(using = TimestampDeserializer.class)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty(required = false)
	private String updatedAt;

	@JsonDeserialize(using = TimestampDeserializer.class)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty(required = false)
	private String createdAt;

	public SMDate() {
	}

	public String getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt.toString();
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

}
