package com.zill.service.model;

public class SMRestaurantLang extends SMDate {

	private String langCode;

	private String information;

	public SMRestaurantLang() {

	}

	public String getLangCode() {
		return langCode;
	}

	public void setLangCode(String langCode) {
		this.langCode = langCode;
	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}
}
