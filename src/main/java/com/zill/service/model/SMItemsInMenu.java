package com.zill.service.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class SMItemsInMenu extends SMDate {

	private Long menuItemAsLong;

	private int itemDiscountRate;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private double itemPrice;

	@JsonProperty(required = false)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private SMMenuProduct menuItemAsMenuItem;

	public SMItemsInMenu() {

	}

	public Long getMenuItem() {
		return menuItemAsLong;
	}

	public void setMenuItem(Long menuItem) {
		this.menuItemAsLong = menuItem;
	}

	public int getItemDiscountRate() {
		return itemDiscountRate;
	}

	public void setItemDiscountRate(int itemDiscountRate) {
		this.itemDiscountRate = itemDiscountRate;
	}

	public double getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}

	public SMMenuProduct getMenuItemAsMenuItem() {
		return menuItemAsMenuItem;
	}

	public void setMenuItemAsMenuItem(SMMenuProduct menuItemAsMenuItem) {
		this.menuItemAsMenuItem = menuItemAsMenuItem;
	}
}
