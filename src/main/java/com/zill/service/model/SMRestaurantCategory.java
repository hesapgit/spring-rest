package com.zill.service.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SMRestaurantCategory extends SMDate {

	@JsonProperty(required = false)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String id;

	private String genericname;

	@JsonProperty(required = false)
	private List<SMRestaurantCategoryLang> translations;

	@JsonProperty(required = false)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<SMRestaurant> restaurants;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getGenericname() {
		return genericname;
	}

	public void setGenericname(String genericname) {
		this.genericname = genericname;
	}

	public List<SMRestaurantCategoryLang> getTranslations() {
		return translations;
	}

	public void setTranslations(List<SMRestaurantCategoryLang> translations) {
		this.translations = translations;
	}

	public List<SMRestaurant> getRestaurants() {
		return restaurants;
	}

	public void setRestaurants(List<SMRestaurant> restaurants) {
		this.restaurants = restaurants;
	}

}
