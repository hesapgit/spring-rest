package com.zill.service.model;

public class SMRestaurantCategoryLang extends SMDate {

	private String langCode;

	private String Translatedname;

	private String description;

	public String getLangCode() {
		return langCode;
	}

	public void setLangCode(String langCode) {
		this.langCode = langCode;
	}

	public String getTranslatedname() {
		return Translatedname;
	}

	public void setTranslatedname(String translatedname) {
		Translatedname = translatedname;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
