package com.zill.service.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SMAccount extends SMDate {

	@JsonProperty(required = false)
	private String id;

	private String username;

	private String password;

	private String role;

	private String email;
	
	private SMProfile profile;
	
	private SMAccountStatus accountStatus;
	
	private List<Long> userMenu;
	
	private List<Long> userRestaurant;
	/**
	 * 
	 */
	public SMAccount() {

	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}




	public SMProfile getProfile() {
		return profile;
	}

	public void setProfile(SMProfile profile) {
		this.profile = profile;
	}

	public SMAccountStatus getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(SMAccountStatus accountStatus) {
		this.accountStatus = accountStatus;
	}

	public List<Long> getUserMenu() {
		return userMenu;
	}

	public void setUserMenu(List<Long> userMenu) {
		this.userMenu = userMenu;
	}

	public List<Long> getUserRestaurant() {
		return userRestaurant;
	}

	public void setUserRestaurant(List<Long> userRestaurant) {
		this.userRestaurant = userRestaurant;
	}

}
