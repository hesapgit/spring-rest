package com.zill.service.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SMMenuProduct extends SMDate {

	@JsonProperty(required = false)
	private String id;

	private Integer listOrder;

	private String currencyCode;

	private Double price;

	@JsonProperty(required = false)
	private Integer serviceTime;

	private String groupId;

	private Boolean isHelal;

	@JsonProperty(required = false)
	private Integer calori;

	private List<Long> ingredients;

	private List<SMMenuProductLang> productTranslations;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty(required = false)
	private List<SMMenuItemImage> menuItemImages;

	public SMMenuProduct() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Integer getServiceTime() {
		return serviceTime;
	}

	public void setServiceTime(Integer serviceTime) {
		this.serviceTime = serviceTime;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public Boolean getIsHelal() {
		return isHelal;
	}

	public void setIsHelal(Boolean isHelal) {
		this.isHelal = isHelal;
	}

	public Integer getCalori() {
		return calori;
	}

	public void setCalori(Integer calori) {
		this.calori = calori;
	}

	public List<SMMenuProductLang> getProductTranslations() {
		return productTranslations;
	}

	public void setProductTranslations(List<SMMenuProductLang> productTranslations) {
		this.productTranslations = productTranslations;
	}

	public List<Long> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Long> ingredients) {
		this.ingredients = ingredients;
	}

	public Integer getListOrder() {
		return listOrder;
	}

	public void setListOrder(Integer listOrder) {
		this.listOrder = listOrder;
	}

	public List<SMMenuItemImage> getMenuItemImages() {
		return menuItemImages;
	}

	public void setMenuItemImages(List<SMMenuItemImage> menuItemImages) {
		this.menuItemImages = menuItemImages;
	}
}
