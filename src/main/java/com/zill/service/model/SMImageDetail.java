package com.zill.service.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SMImageDetail extends SMDate {

	@JsonProperty(required = false)
	private String id;

	@JsonProperty(required = false)
	private String referenceImagePath;

	@JsonProperty(required = false)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Integer size;

	private String imagePath;

	private String description;

	/**
	 * 
	 */
	public SMImageDetail() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getReferenceImagePath() {
		return referenceImagePath;
	}

	public void setReferenceImagePath(String referenceImagePath) {
		this.referenceImagePath = referenceImagePath;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

}
