package com.zill.service.model;

public class SMMenuGroupLang extends SMDate {

	private String langCode;

	private String name;

	private String description;

	public SMMenuGroupLang() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLangCode() {
		return langCode;
	}

	public void setLangCode(String langCode) {
		this.langCode = langCode;
	}
}
