package com.zill.service.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SMMenu extends SMDate {

	@JsonProperty(required = false)
	private String id;

	private String genericname;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private double specialOffer;

	@JsonProperty(required = false)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<String> menuImages;

	@JsonProperty(required = false)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<SMItemsInMenu> itemsInMenus;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Long restaurant;

	@JsonProperty(required = false)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<SMMenuLang> menuTranslations;

	@JsonProperty(required = false)
	private Long parentMenuId;

	public SMMenu() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getGenericname() {
		return genericname;
	}

	public void setGenericname(String genericname) {
		this.genericname = genericname;
	}

	public List<String> getMenuImages() {
		return menuImages;
	}

	public void setMenuImages(List<String> menuImages) {
		this.menuImages = menuImages;
	}

	public double getSpecialOffer() {
		return specialOffer;
	}

	public void setSpecialOffer(double specialOffer) {
		this.specialOffer = specialOffer;
	}

	public List<SMItemsInMenu> getItemsInMenus() {
		return itemsInMenus;
	}

	public void setItemsInMenus(List<SMItemsInMenu> itemsInMenus) {
		this.itemsInMenus = itemsInMenus;
	}

	public Long getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Long restaurant) {
		this.restaurant = restaurant;
	}

	public List<SMMenuLang> getMenuTranslations() {
		return menuTranslations;
	}

	public void setMenuTranslations(List<SMMenuLang> menuTranslations) {
		this.menuTranslations = menuTranslations;
	}

	public Long getParentMenuId() {
		return parentMenuId;
	}

	public void setParentMenuId(Long parentMenuId) {
		this.parentMenuId = parentMenuId;
	}

}
