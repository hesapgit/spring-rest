package com.zill.service.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SMRestaurantImage extends SMImageDetail {

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty(required = false)
	private Long restaurant;

	public SMRestaurantImage() {

	}

	public Long getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Long restaurant) {
		this.restaurant = restaurant;
	}

}
