package com.zill.service.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SMMenuGroup extends SMDate {

	@JsonProperty(required = false)
	private String id;

	private String code;

	private List<SMMenuGroupLang> groupTranslations;

	public SMMenuGroup() {

	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<SMMenuGroupLang> getGroupTranslations() {
		return groupTranslations;
	}

	public void setGroupTranslations(List<SMMenuGroupLang> groupTranslations) {
		this.groupTranslations = groupTranslations;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
