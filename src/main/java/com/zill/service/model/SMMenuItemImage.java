package com.zill.service.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SMMenuItemImage extends SMImageDetail {

	@JsonProperty(required = false)
	private Long menuItem;

	public SMMenuItemImage() {

	}

	public Long getMenuItem() {
		return menuItem;
	}

	public void setMenuItem(Long menuItem) {
		this.menuItem = menuItem;
	}

}
