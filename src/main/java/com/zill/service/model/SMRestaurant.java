package com.zill.service.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SMRestaurant extends SMDate {

	@JsonProperty(required = false)
	private String id;

	private String name;

	private String defaultLangCode;

	@JsonProperty(required = false)
	private List<SMRestaurantLang> translations;

	@JsonProperty(required = false)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<SMRestaurantImage> restaurantImages;

	@JsonProperty(required = false)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<SMMenu> menu;

	private SMRestaurantCategory restaurantCategory;

	public SMRestaurant() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDefaultLangCode() {
		return defaultLangCode;
	}

	public void setDefaultLangCode(String defaultLangCode) {
		this.defaultLangCode = defaultLangCode;
	}

	public List<SMRestaurantLang> getTranslations() {
		return translations;
	}

	public void setTranslations(List<SMRestaurantLang> translations) {
		this.translations = translations;
	}

	public List<SMMenu> getMenu() {
		return menu;
	}

	public void setMenu(List<SMMenu> menu) {
		this.menu = menu;
	}

	public SMRestaurantCategory getRestaurantCategory() {
		return restaurantCategory;
	}

	public void setRestaurantCategory(SMRestaurantCategory restaurantCategory) {
		this.restaurantCategory = restaurantCategory;
	}
}
