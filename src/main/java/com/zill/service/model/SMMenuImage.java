package com.zill.service.model;

public class SMMenuImage extends SMImageDetail {

	private Long menu;

	public SMMenuImage() {
		super();
	}

	public Long getMenu() {
		return menu;
	}

	public void setMenu(Long menu) {
		this.menu = menu;
	}

}
