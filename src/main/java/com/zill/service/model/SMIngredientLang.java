package com.zill.service.model;

public class SMIngredientLang extends SMDate {

	private String langCode;

	private String Translatedname;

	public SMIngredientLang() {

	}

	public String getLangCode() {
		return langCode;
	}

	public void setLangCode(String langCode) {
		this.langCode = langCode;
	}

	public String getTranslatedname() {
		return Translatedname;
	}

	public void setTranslatedname(String transname) {
		Translatedname = transname;
	}

}
