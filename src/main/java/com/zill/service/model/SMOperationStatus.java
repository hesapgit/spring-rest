package com.zill.service.model;

import com.fasterxml.jackson.annotation.JsonInclude;

public class SMOperationStatus {
	private Long recordId;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String referenceId;

	private Boolean isSuccessful;

	private String message;

	public SMOperationStatus() {

	}

	public SMOperationStatus(Boolean isSuccessful) {
		this.isSuccessful = isSuccessful;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Boolean getIsSuccessful() {
		return isSuccessful;
	}

	public void setIsSuccessful(Boolean isSuccessful) {
		this.isSuccessful = isSuccessful;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}
}
