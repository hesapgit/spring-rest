package com.zill.service.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SMIngredient extends SMDate {

	@JsonProperty(required = false)
	private String id;

	private String genericname;

	@JsonProperty(required = false)
	private List<SMIngredientLang> translations;

	public SMIngredient() {

	}

	public String getId() {
		return id;
	}

	public String getGenericname() {
		return genericname;
	}

	public List<SMIngredientLang> getTranslations() {
		return translations;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setGenericname(String genericname) {
		this.genericname = genericname;
	}

	public void setTranslations(List<SMIngredientLang> translations) {
		this.translations = translations;
	}

}
