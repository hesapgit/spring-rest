package com.zill.service.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SMProfile extends SMDate {
	
	
	@JsonProperty(required = false)
	private String id;

	private String firstName;

	private String fullName;

	private String lastName;
	
	// responsible
		private List<Long> menuId;

		private List<Long> restaurantId;
		
		

		public SMProfile() {
			
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getFullName() {
			return fullName;
		}

		public void setFullName(String fullName) {
			this.fullName = fullName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public List<Long> getMenuId() {
			return menuId;
		}

		public void setMenuId(List<Long> menuId) {
			this.menuId = menuId;
		}

		public List<Long> getRestaurantId() {
			return restaurantId;
		}

		public void setRestaurantId(List<Long> restaurantId) {
			this.restaurantId = restaurantId;
		}
}
