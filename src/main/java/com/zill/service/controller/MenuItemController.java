package com.zill.service.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zill.data.dao.CurrencyRepository;
import com.zill.data.dao.LanguageRepository;
import com.zill.data.dao.MenuGroupRepository;
import com.zill.data.dao.MenuItemIngredientRepository;
import com.zill.data.dao.MenuItemRepository;
import com.zill.data.dao.MenuItemTranslationRepository;
import com.zill.data.dao.RestaurantRepository;
import com.zill.data.model.MenuItem;
import com.zill.data.model.MenuItemIngredient;
import com.zill.data.model.MenuItemTranslation;
import com.zill.service.model.SMMenuProduct;
import com.zill.service.model.SMMenuProductLang;
import com.zill.service.model.SMOperationStatus;
import com.zill.util.RootContexts;

@RestController
@RequestMapping(path = RootContexts.MENU_ITEM_ROOT_PATH)
@Consumes("application/json; charset=UTF-8")
@Produces("application/json; charset=UTF-8")
public class MenuItemController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private MenuItemRepository menuItemRepo;

	@Autowired
	private MenuItemTranslationRepository menuItemTranslationRepo;

	@Autowired
	private CurrencyRepository currencyRepo;

	@Autowired
	private RestaurantRepository restaurantRepo;

	@Autowired
	private MenuGroupRepository menuGroupRepo;

	@Autowired
	private LanguageRepository languageRepo;
	// requestbody
	// array midir
	@Autowired
	private MenuItemIngredientRepository menuIngredientRepo;

	private static final String PRODUCT_ID = "productId";

	private static final String INGREDIENT_ID = "ingredientId";

	private static final String PRODUCT_PATH = "/products";

	private static final String PRODUCT_ID_PATH = PRODUCT_PATH + "/{" + PRODUCT_ID + "}";

	// private static final String PRODUCT_INGREDIENT_ID_PATH = PRODUCT_ID_PATH
	// + "/ingredient";

	private static final String PRODUCT_INGREDIENT_ID_PATH = PRODUCT_ID_PATH + "/ingredient/{" + INGREDIENT_ID + "}";

	@RequestMapping(path = PRODUCT_PATH + "/{" + RootContexts.LANGUAGE_CODE + "}/", method = RequestMethod.GET)
	public List<SMMenuProduct> getMenuProductsWithSelectedLanguage(
			@PathVariable(RootContexts.LANGUAGE_CODE) String languageCode,
			@PathVariable(RootContexts.RESTAURANT_ID) Long restaurantId) {
		logger.debug("Get all products requested for Restaurant: {}", restaurantId);
		List<MenuItem> selectedItems = new ArrayList<>();
		selectedItems = menuItemRepo.findByRestaurantId(restaurantId);

		List<SMMenuProduct> responseContent = new ArrayList<>();
		for (MenuItem menuItem : selectedItems) {
			SMMenuProduct responseItem = new SMMenuProduct();
			responseItem.setCalori(menuItem.getCalori());
			responseItem.setCurrencyCode(menuItem.getCurrencyBean().getCode());
			responseItem.setGroupId(menuItem.getMenuGroup().getId().toString());
			responseItem.setIsHelal(menuItem.getIsHelal());
			responseItem.setPrice(menuItem.getPrice());
			responseItem.setServiceTime(menuItem.getServiceTime());
			responseItem.setUpdatedAt(menuItem.getUpdatedAt().toString());
			responseItem.setCreatedAt(menuItem.getCreatedAt().toString());
			responseItem.setProductTranslations(new ArrayList<>());
			// ingredient arraylist
			responseItem.setIngredients(new ArrayList<>());
			responseItem.setListOrder(menuItem.getListOrder());

			for (MenuItemTranslation menuTranslation : menuItem.getMenuItemTranslations()) {
				if (menuTranslation.getLanguage().getCode().equals(languageCode)) {
					SMMenuProductLang responseItemLan = new SMMenuProductLang();
					responseItemLan.setDescription(menuTranslation.getDescription());
					responseItemLan.setName(menuTranslation.getName());

					responseItem.getProductTranslations().add(responseItemLan);
					break;
				}
			}

			// içindekilere ait tüm bileşenleri ekleyelim
			for (MenuItemIngredient menuItemIngredient : menuIngredientRepo
					.findIngredientsByMenuItemId(menuItem.getId())) {
				responseItem.getIngredients().add(menuItemIngredient.getIngredientId());
			}

			responseContent.add(responseItem);
		}

		return responseContent;
	}

	@RequestMapping(path = PRODUCT_PATH, method = RequestMethod.GET)
	public List<SMMenuProduct> getAllProductsForRestaurant(
			@PathVariable(RootContexts.RESTAURANT_ID) Long restaurantId) {
		logger.debug("Get all products requested for Restaurant: {}", restaurantId);

		// önce restorana bağlı tüm ürünleri çekelim
		List<MenuItem> selectedItems = menuItemRepo.findByRestaurantId(restaurantId);

		List<SMMenuProduct> responseContent = new ArrayList<>();

		// içindekiler dışındaki tüm bileşenleri dolduralım
		for (MenuItem menuItem : selectedItems) {
			SMMenuProduct responseItem = new SMMenuProduct();
			responseItem.setId(menuItem.getId().toString());
			responseItem.setCalori(menuItem.getCalori());
			responseItem.setCurrencyCode(menuItem.getCurrencyBean().getCode());
			responseItem.setGroupId(menuItem.getMenuGroup().getId().toString());
			responseItem.setIsHelal(menuItem.getIsHelal());
			responseItem.setPrice(menuItem.getPrice());
			responseItem.setServiceTime(menuItem.getServiceTime());
			responseItem.setProductTranslations(new ArrayList<>());
			responseItem.setListOrder(menuItem.getListOrder());
			responseItem.setIngredients(new ArrayList<>());
			responseItem.setUpdatedAt(menuItem.getUpdatedAt().toString());
			responseItem.setCreatedAt(menuItem.getCreatedAt().toString());

			for (MenuItemTranslation menuItemTranslation : menuItem.getMenuItemTranslations()) {
				SMMenuProductLang responseItemLan = new SMMenuProductLang();
				responseItemLan.setDescription(menuItemTranslation.getDescription());
				responseItemLan.setLangCode(menuItemTranslation.getLanguage().getCode());
				responseItemLan.setName(menuItemTranslation.getName());
				responseItem.getProductTranslations().add(responseItemLan);
			}

			// içindekilere ait tüm bileşenleri ekleyelim
			for (MenuItemIngredient menuItemIngredient : menuIngredientRepo
					.findIngredientsByMenuItemId(menuItem.getId())) {
				responseItem.getIngredients().add(menuItemIngredient.getIngredientId());
			}

			responseContent.add(responseItem);
		}

		return responseContent;
	}

	@RequestMapping(path = PRODUCT_PATH, method = RequestMethod.POST)
	public SMOperationStatus addMenuProduct(@PathVariable(RootContexts.RESTAURANT_ID) Long restaurantId,
			@RequestBody SMMenuProduct productToAdd) {
		SMOperationStatus status = new SMOperationStatus();

		// en az bir dil olması gerekiyor onu kontrol edelim
		if (productToAdd.getProductTranslations() == null || productToAdd.getProductTranslations().size() == 0) {
			status.setIsSuccessful(false);
			status.setMessage("At least one translation required!!!");
			return status;
		}

		MenuItem newMenuItem = new MenuItem();
		newMenuItem.setMenuItemTranslations(new ArrayList<>());

		newMenuItem.setCalori(productToAdd.getCalori());
		newMenuItem.setCurrencyBean(currencyRepo.findByCode(productToAdd.getCurrencyCode()));
		newMenuItem.setPrice(productToAdd.getPrice());
		newMenuItem.setIsHelal(productToAdd.getIsHelal());
		newMenuItem.setMenuGroup(menuGroupRepo.findOne(new Long(productToAdd.getGroupId())));
		newMenuItem.setServiceTime(productToAdd.getServiceTime());
		newMenuItem.setRestaurant(restaurantRepo.findOne(restaurantId));
		newMenuItem.setName(productToAdd.getProductTranslations().get(0).getName());
		newMenuItem.setListOrder(productToAdd.getListOrder());

		for (SMMenuProductLang productLang : productToAdd.getProductTranslations()) {
			MenuItemTranslation newMenuItemTranslation = new MenuItemTranslation();
			newMenuItemTranslation.setLanguage(languageRepo.findByCode(productLang.getLangCode()));
			newMenuItemTranslation.setDescription(productLang.getDescription());
			newMenuItemTranslation.setName(productLang.getName());
			newMenuItemTranslation.setMenuItem(newMenuItem);

			newMenuItem.getMenuItemTranslations().add(newMenuItemTranslation);
		}

		try {
			newMenuItem = menuItemRepo.saveAndFlush(newMenuItem);

			List<MenuItemIngredient> ingredients = new ArrayList<>();

			// içindekileri ekle
			for (Long ingredientId : productToAdd.getIngredients()) {
				MenuItemIngredient menuIngredient = new MenuItemIngredient();
				menuIngredient.setMenuItemId(newMenuItem.getId());
				menuIngredient.setIngredientId(ingredientId);
				ingredients.add(menuIngredient);
			}

			if (ingredients.size() > 0) {
				menuIngredientRepo.save(ingredients);
			}

		} catch (Exception e) {
			status.setIsSuccessful(false);
			status.setMessage("Error occured while recording to DB!");
			return status;
		}

		status.setIsSuccessful(true);
		status.setRecordId(newMenuItem.getId());

		return status;
	}

	/**
	 * Sadece dile bağlı olmayan değişiklikleri güncellemek için kullanılır.
	 * 
	 * @param restaurantId
	 * @param productToUpdate
	 * @return
	 */
	@RequestMapping(path = PRODUCT_ID_PATH, method = RequestMethod.POST)
	public SMOperationStatus updateMenuProduct(@PathVariable(PRODUCT_ID) Long menuItemId,
			@RequestBody SMMenuProduct productToUpdate) {
		SMOperationStatus status = new SMOperationStatus();
		SMOperationStatus statusIngredient = new SMOperationStatus(true);
		MenuItem selectedMenuItem = menuItemRepo.getOne(menuItemId);

		if (selectedMenuItem == null) {
			status.setIsSuccessful(false);
			status.setMessage("Requested Product can not found!!!");
			return status;
		}

		selectedMenuItem.setMenuItemTranslations(new ArrayList<>());

		selectedMenuItem.setCalori(productToUpdate.getCalori());
		selectedMenuItem.setCurrencyBean(currencyRepo.findByCode(productToUpdate.getCurrencyCode()));
		selectedMenuItem.setPrice(productToUpdate.getPrice());
		selectedMenuItem.setIsHelal(productToUpdate.getIsHelal());
		selectedMenuItem.setMenuGroup(menuGroupRepo.findOne(new Long(productToUpdate.getGroupId())));
		selectedMenuItem.setServiceTime(productToUpdate.getServiceTime());
		selectedMenuItem.setListOrder(productToUpdate.getListOrder());

		selectedMenuItem = menuItemRepo.saveAndFlush(selectedMenuItem);

		if (productToUpdate.getIngredients() != null) {

			statusIngredient = this.updateMenuIngredients(menuItemId, productToUpdate);

			// List <MenuItemIngredient> selectedIngredients =
			// menuIngredientRepo.findIngredientsByMenuItemId(menuItemId);
			//
			// MenuItemIngredient temp;
			// List<MenuItemIngredient> ingredients = new ArrayList<>();
			//
			// //update ingredient
			// try{
			// outerLoop1:
			// for(Long toAddIngre:productToAdd.getIngredients()){
			// temp = new MenuItemIngredient();
			// for(MenuItemIngredient oneIngre:selectedIngredients){
			// if(toAddIngre == oneIngre.getIngredientId()){
			// continue outerLoop1;
			// }
			// }
			// temp.setIngredientId(toAddIngre);
			// temp.setMenuItemId(menuItemId);
			// ingredients.add(temp);
			// }
			// if (ingredients.size() > 0) {
			// menuIngredientRepo.save(ingredients);
			// }
			//
			// List <MenuItemIngredient> selectedIngredients2 =
			// menuIngredientRepo.findIngredientsByMenuItemId(menuItemId);
			// outerLoop2:
			// for(MenuItemIngredient oneIngre:selectedIngredients2){
			// for(Long toAddIngre:productToAdd.getIngredients()){
			// if(toAddIngre == oneIngre.getIngredientId()){
			// continue outerLoop2;
			// }
			// }
			// menuIngredientRepo.delete(oneIngre.getId());
			// }
			//
			//
			//
			// } catch (Exception e) {
			// status.setIsSuccessful(false);
			// status.setMessage("Error occured while recording to DB!");
			// return status;
			// }
			// //update ingredient ends
			//
		}

		if (statusIngredient.getIsSuccessful()) {
			status.setIsSuccessful(true);
			status.setRecordId(selectedMenuItem.getId());
		} else {
			status.setIsSuccessful(false);
			status.setMessage("Ingredients could not be updated");
		}
		return status;
	}

	@RequestMapping(path = PRODUCT_ID_PATH + "/translations", method = RequestMethod.POST)
	public SMOperationStatus updateMenuProductTranslations(@PathVariable(PRODUCT_ID) Long menuItemId,
			@RequestBody List<SMMenuProductLang> productTranslations) {
		SMOperationStatus status = new SMOperationStatus();

		MenuItem selectedMenuItem = menuItemRepo.getOne(menuItemId);

		if (selectedMenuItem == null) {
			status.setIsSuccessful(false);
			status.setMessage("Requested Product can not found!!!");
			return status;
		}

		Map<String, SMMenuProductLang> mapLangTranslation = new HashMap<>();

		for (SMMenuProductLang productLang : productTranslations) {
			mapLangTranslation.put(productLang.getLangCode(), productLang);
		}

		for (MenuItemTranslation itemTranslation : selectedMenuItem.getMenuItemTranslations()) {
			SMMenuProductLang productLang = mapLangTranslation.get(itemTranslation.getLanguage().getCode());
			if (productLang != null) {
				itemTranslation.setDescription(productLang.getDescription());
				itemTranslation.setName(productLang.getName());
				mapLangTranslation.remove(productLang.getLangCode());
			}
		}

		// yeni eklenecek çeviri var mı kontrol edelim
		for (SMMenuProductLang productLang : mapLangTranslation.values()) {
			MenuItemTranslation itemTranslation = new MenuItemTranslation();
			itemTranslation.setLanguage(languageRepo.getOne(productLang.getLangCode()));
			itemTranslation.setDescription(productLang.getDescription());
			itemTranslation.setName(productLang.getName());
			itemTranslation.setMenuItem(selectedMenuItem);

			selectedMenuItem.getMenuItemTranslations().add(itemTranslation);
		}

		selectedMenuItem = menuItemRepo.saveAndFlush(selectedMenuItem);
		status.setIsSuccessful(true);
		status.setRecordId(selectedMenuItem.getId());

		return status;
	}

	@RequestMapping(path = PRODUCT_ID_PATH + "/translations/{" + RootContexts.LANGUAGE_CODE
			+ "}", method = RequestMethod.POST)
	public SMOperationStatus removeMenuProductTranslations(@PathVariable(PRODUCT_ID) Long menuItemId,
			@PathVariable(RootContexts.LANGUAGE_CODE) String languageCode) {

		SMOperationStatus status = new SMOperationStatus();
		status.setIsSuccessful(false);

		MenuItem menuItem = menuItemRepo.getOne(menuItemId);

		if (menuItem == null) {
			status.setIsSuccessful(false);
			status.setMessage("Requested Product can not be found!!!");
			return status;
		}

		// veri modellerimizin cascede tipi ayarları ilişkisel verilerin
		// öncelikle parent bileşenlerden kaldırılmasını zorluyor. Bu yüzden
		// önce restoranın içinden istenilen dil tercihi kaldırıalcak.

		for (MenuItemTranslation itemTranslation : menuItem.getMenuItemTranslations()) {
			if (itemTranslation.getLanguage().getCode().equals(languageCode)) {
				// menu item dan translation çıkar
				menuItem.getMenuItemTranslations().remove(itemTranslation);

				// translation reposuna silme komutu ver
				menuItemTranslationRepo.delete(itemTranslation);

				status.setIsSuccessful(true);
				break;
			}
		}

		return status;
	}

	@RequestMapping(path = PRODUCT_ID_PATH + "/remove", method = RequestMethod.POST)
	public SMOperationStatus deleteMenuProduct(@PathVariable(RootContexts.RESTAURANT_ID) Long restaurantId,
			@PathVariable(PRODUCT_ID) Long productId) {
		SMOperationStatus operationStatus = new SMOperationStatus();
		menuItemRepo.delete(productId);
		operationStatus.setIsSuccessful(true);
		return operationStatus;
	}

	@RequestMapping(path = PRODUCT_ID_PATH + "/get", method = RequestMethod.GET)
	public SMMenuProduct getOneProductForRestaurant(@PathVariable(PRODUCT_ID) Long productId) {
		logger.debug("Get all products requested for product: {}", PRODUCT_ID);

		MenuItem menuItem = menuItemRepo.getOne(productId);

		SMMenuProduct responseItem = new SMMenuProduct();
		responseItem.setId(menuItem.getId().toString());
		responseItem.setCalori(menuItem.getCalori());
		responseItem.setCurrencyCode(menuItem.getCurrencyBean().getCode());
		responseItem.setGroupId(menuItem.getMenuGroup().getId().toString());
		responseItem.setIsHelal(menuItem.getIsHelal());
		responseItem.setPrice(menuItem.getPrice());
		responseItem.setServiceTime(menuItem.getServiceTime());
		responseItem.setProductTranslations(new ArrayList<>());
		responseItem.setListOrder(menuItem.getListOrder());
		responseItem.setIngredients(new ArrayList<>());
		responseItem.setUpdatedAt(menuItem.getUpdatedAt().toString());
		responseItem.setCreatedAt(menuItem.getCreatedAt().toString());

		for (MenuItemTranslation menuItemTranslation : menuItemTranslationRepo.findByMenuItemId(productId)) {
			logger.debug("menu item translation size: {}", menuItem.getMenuItemTranslations().size());
			logger.debug("menu item translation id: {}", menuItemTranslation.getId());
			SMMenuProductLang responseItemLan = new SMMenuProductLang();
			responseItemLan.setDescription(menuItemTranslation.getDescription());
			responseItemLan.setLangCode(menuItemTranslation.getLanguage().getCode());
			responseItemLan.setName(menuItemTranslation.getName());
			responseItem.getProductTranslations().add(responseItemLan);
		}

		for (MenuItemIngredient menuItemIngredient : menuIngredientRepo.findIngredientsByMenuItemId(menuItem.getId())) {
			responseItem.getIngredients().add(menuItemIngredient.getIngredientId());
		}

		return responseItem;
	}

	@RequestMapping(path = PRODUCT_ID_PATH + "/updateingredients", method = RequestMethod.POST)
	public SMOperationStatus updateMenuIngredients(@PathVariable(PRODUCT_ID) Long menuItemId,
			@RequestBody SMMenuProduct productToAdd) {
		SMOperationStatus status = new SMOperationStatus();

		List<MenuItemIngredient> selectedIngredients = menuIngredientRepo.findIngredientsByMenuItemId(menuItemId);
		MenuItemIngredient temp;
		List<MenuItemIngredient> ingredients = new ArrayList<>();

		//add new 
		try {
			outerLoop1: for (Long toAddIngre : productToAdd.getIngredients()) {
				temp = new MenuItemIngredient();
				for (MenuItemIngredient selectedOneIngre : selectedIngredients) {
					if (toAddIngre == selectedOneIngre.getIngredientId()) {
						continue outerLoop1;
					}
				}
				temp.setIngredientId(toAddIngre);
				temp.setMenuItemId(menuItemId);
				ingredients.add(temp);
			}
			if (ingredients.size() > 0) {
				menuIngredientRepo.save(ingredients);
			}

			//delete extras
			List<MenuItemIngredient> selectedIngredients2 = menuIngredientRepo.findIngredientsByMenuItemId(menuItemId);
			outerLoop2: for (MenuItemIngredient oneIngre : selectedIngredients2) {
				for (Long toAddIngre : productToAdd.getIngredients()) {
					if (toAddIngre == oneIngre.getIngredientId()) {
						continue outerLoop2;
					}
				}
				menuIngredientRepo.delete(oneIngre.getId());
			}

		} catch (Exception e) {
			status.setIsSuccessful(false);
			status.setMessage("Error occured while recording to DB!");
			return status;
		}

		status.setIsSuccessful(true);

		return status;
	}

	@RequestMapping(path = PRODUCT_INGREDIENT_ID_PATH + "/add", method = RequestMethod.POST)
	public SMOperationStatus addMenuIngredients(@PathVariable(PRODUCT_ID) Long menuItemId,
			@PathVariable(INGREDIENT_ID) Long ingredientId) {
		SMOperationStatus status = new SMOperationStatus();

		List<MenuItemIngredient> selectedIngredients = menuIngredientRepo.findIngredientsByMenuItemId(menuItemId);

		List<MenuItemIngredient> ingredients = new ArrayList<>();

		try {

			for (MenuItemIngredient oneIngre : selectedIngredients) {
				if (ingredientId == oneIngre.getIngredientId()) {
					status.setIsSuccessful(false);
					status.setMessage("Ingredient has already exist!");
					return status;
				}

			}
			MenuItemIngredient temp = new MenuItemIngredient();
			temp.setIngredientId(ingredientId);
			temp.setMenuItemId(menuItemId);
			ingredients.add(temp);
			if (ingredients.size() > 0) {
				menuIngredientRepo.save(ingredients);
			}

		} catch (Exception e) {
			status.setIsSuccessful(false);
			status.setMessage("Error occured while recording to DB!");
			return status;
		}

		status.setIsSuccessful(true);

		
		
		return status;
	}

	@RequestMapping(path = PRODUCT_INGREDIENT_ID_PATH + "/remove", method = RequestMethod.POST)
	public SMOperationStatus removeMenuIngredients(@PathVariable(PRODUCT_ID) Long menuItemId,
			@PathVariable(INGREDIENT_ID) Long ingredientId) {
		SMOperationStatus status = new SMOperationStatus();

		List<MenuItemIngredient> selectedIngredients = menuIngredientRepo.findIngredientsByMenuItemId(menuItemId);

		try {

			for (MenuItemIngredient oneIngre : selectedIngredients) {
				if (ingredientId == oneIngre.getIngredientId()) {
					menuIngredientRepo.delete(oneIngre);
					status.setIsSuccessful(true);
					status.setMessage("Ingredient deleted");
					return status;
				}

			}

		} catch (Exception e) {
			status.setIsSuccessful(false);
			status.setMessage("Error occured while recording to DB!");
			return status;
		}

		status.setIsSuccessful(false);
		status.setMessage("Ingredient not exist");

		return status;
	}

}
