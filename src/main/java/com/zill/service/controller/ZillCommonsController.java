package com.zill.service.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zill.data.dao.CurrencyRepository;
import com.zill.data.dao.LanguageRepository;
import com.zill.data.model.Currency;
import com.zill.data.model.Language;
import com.zill.service.model.SMCurrency;
import com.zill.service.model.SMLanguage;
import com.zill.util.RootContexts;

@RestController
@RequestMapping(path = RootContexts.SERVICE_COMMONS_PATH)
public class ZillCommonsController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final String LANGUAGE_PATH = "/availableLanguages";

	private static final String CURRENCY_PATH = "/currencies";

	@Autowired
	LanguageRepository languageRepo;

	@Autowired
	CurrencyRepository currencyRepo;

	@RequestMapping(path = LANGUAGE_PATH, method = RequestMethod.GET)
	public List<SMLanguage> getMenuLanguages() {
		logger.debug("Get all available languages requested");
		List<Language> languages = languageRepo.findAll();
		List<SMLanguage> languagesResponse = new ArrayList<>();
		for (Language lang : languages) {
			SMLanguage responseItem = new SMLanguage();
			responseItem.setCode(lang.getCode());
			responseItem.setName(lang.getName());
			responseItem.setUpdatedAt(lang.getUpdatedAt().toString());
			languagesResponse.add(responseItem);
		}
		return languagesResponse;
	}

	@RequestMapping(path = CURRENCY_PATH, method = RequestMethod.GET)
	public List<SMCurrency> getAllCurrencies() {
		logger.debug("Get all available currencies requested");
		List<Currency> currencies = currencyRepo.findAll();
		List<SMCurrency> currenciesResponse = new ArrayList<>();
		for (Currency currency : currencies) {
			SMCurrency responseItem = new SMCurrency();
			responseItem.setCode(currency.getCode());
			responseItem.setName(currency.getName());
			responseItem.setSymbol(currency.getSymbol());
			responseItem.setUpdatedAt(currency.getUpdatedAt().toString());
			currenciesResponse.add(responseItem);
		}
		return currenciesResponse;
	}
}
