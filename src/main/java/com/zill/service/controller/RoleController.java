package com.zill.service.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zill.data.dao.RoleRepository;
import com.zill.data.model.Role;
import com.zill.service.model.SMRole;
import com.zill.util.RootContexts;

@RestController
@RequestMapping(path = RootContexts.WEB_API_VERSION_1)
@Consumes("application/json; charset=UTF-8")
@Produces("application/json; charset=UTF-8")
public class RoleController {

	@Autowired
	private RoleRepository roleRepository;

	@RequestMapping(path = "/roles", method = RequestMethod.GET)
	public List<SMRole> getRoles() {

		List<Role> roles = roleRepository.findAll();
		List<SMRole> responseContent = new ArrayList<>();

		for (Role role : roles) {
			SMRole temp = new SMRole();
			temp.setLabel(role.getLabel());
			temp.setCode(role.getCode());
			temp.setId(role.getId());
			responseContent.add(temp);
		}

		return responseContent;
	}

}
