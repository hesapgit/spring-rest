package com.zill.service.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zill.data.dao.ItemsInMenusRepository;
import com.zill.data.dao.LanguageRepository;
import com.zill.data.dao.MenuItemIngredientRepository;
import com.zill.data.dao.MenuItemRepository;
import com.zill.data.dao.RestaurantRepository;
import com.zill.data.dao.RestaurantTranslationRepository;
import com.zill.data.model.ItemsInMenus;
import com.zill.data.model.Language;
import com.zill.data.model.Menu;
import com.zill.data.model.MenuItem;
import com.zill.data.model.MenuItemIngredient;
import com.zill.data.model.MenuItemTranslation;
import com.zill.data.model.MenuTranslation;
import com.zill.data.model.Restaurant;
import com.zill.data.model.RestaurantCategoryTranslation;
import com.zill.data.model.RestaurantTranslation;
import com.zill.service.model.SMItemsInMenu;
import com.zill.service.model.SMMenu;
import com.zill.service.model.SMMenuLang;
import com.zill.service.model.SMMenuProduct;
import com.zill.service.model.SMMenuProductLang;
import com.zill.service.model.SMOperationStatus;
import com.zill.service.model.SMRestaurant;
import com.zill.service.model.SMRestaurantCategory;
import com.zill.service.model.SMRestaurantCategoryLang;
import com.zill.service.model.SMRestaurantLang;
import com.zill.util.RootContexts;

@RestController
@RequestMapping(path = RootContexts.WEB_API_VERSION_1)
@Consumes("application/json; charset=UTF-8")
@Produces("application/json; charset=UTF-8")
public class RestaurantController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final String RESTAURANT_ID = "restaurantId";

	private static final String RESTAURANT_PATH_LANG_CODE = "/{" + RootContexts.LANGUAGE_CODE + "}/"
			+ RootContexts.RESTAURANTS_PATH_NAME;

	private static final String RESTAURANT_PATH = "/" + RootContexts.RESTAURANTS_PATH_NAME;

	private static final String RESTAURANT_PATH_WITH_ID = RESTAURANT_PATH + "/{" + RESTAURANT_ID + "}";

	@Autowired
	private RestaurantRepository restaurantRepo;

	@Autowired
	private LanguageRepository langRepo;

	@Autowired
	private RestaurantTranslationRepository restaurantTranslationRepo;

	@Autowired
	private ItemsInMenusRepository itemsInMenusRepo;

	@Autowired
	private MenuItemRepository menuItemRepo;

	@Autowired
	private MenuItemIngredientRepository menuItemIngredientRepo;

	/**
	 * Dil tercihine göre restoranları listelemek için kullanılır.
	 * 
	 * @param languageCode:
	 *            Dil kodu
	 * @return {@link List} of {@link SMRestaurant}
	 */
	@RequestMapping(path = RESTAURANT_PATH_LANG_CODE, method = RequestMethod.GET)
	public List<SMRestaurant> getRestaurantsByLangCode(@PathVariable(RootContexts.LANGUAGE_CODE) String languageCode) {
		logger.debug("Get all available languages requested for Restaurants for Lang Code: {}", languageCode);
		List<SMRestaurant> allRestaurants = new ArrayList<>();
		SMRestaurant temp;
		for (Restaurant res : restaurantRepo.findAll()) {
			temp = new SMRestaurant();
			temp.setDefaultLangCode(res.getLanguage().getCode());
			temp.setId(res.getId().toString());
			temp.setName(res.getName());
			temp.setCreatedAt(res.getCreatedAt().toString().toString());
			temp.setUpdatedAt(res.getUpdatedAt().toString());
			temp.setTranslations(new ArrayList<>());
			if (res.getRestaurantTranslations().size() > 0) {
				for (RestaurantTranslation resTrans : res.getRestaurantTranslations()) {
					if (resTrans.getLanguage().getCode().equals(languageCode)) {
						SMRestaurantLang restLang = new SMRestaurantLang();
						restLang.setInformation(resTrans.getInformation());
						restLang.setLangCode(languageCode);
						temp.getTranslations().add(restLang);
					}
				}
			}
			allRestaurants.add(temp);
		}

		return allRestaurants;
	}

	/**
	 * Dil tercihi olmaksızın tüm restoranları görüntülemek için kullanılır.
	 * 
	 * @return {@link List} of {@link SMRestaurant}
	 */
	@RequestMapping(path = RESTAURANT_PATH, method = RequestMethod.GET)
	public List<SMRestaurant> getAllRestaurants() {
		logger.debug("Get all available languages requested for All Restaurants");
		List<SMRestaurant> allRestaurants = new ArrayList<>();
		SMRestaurant temp;
		for (Restaurant res : restaurantRepo.findAll()) {
			temp = new SMRestaurant();
			temp.setDefaultLangCode(res.getLanguage().getCode());
			temp.setId(res.getId().toString());
			temp.setName(res.getName());
			temp.setCreatedAt(res.getCreatedAt().toString());
			temp.setUpdatedAt(res.getUpdatedAt().toString());
			temp.setTranslations(new ArrayList<>());
			if (res.getRestaurantTranslations().size() > 0) {
				for (RestaurantTranslation resTrans : res.getRestaurantTranslations()) {
					SMRestaurantLang restLang = new SMRestaurantLang();
					restLang.setInformation(resTrans.getInformation());
					restLang.setLangCode(resTrans.getLanguage().getCode());
					temp.getTranslations().add(restLang);
				}
			}
			allRestaurants.add(temp);
		}

		return allRestaurants;
	}

	/**
	 * Bir ya da birden fazla tercüme bilgisi ile yeni bir restoran eklemek için
	 * kullanılır.
	 * 
	 * @param reqRestaurant:
	 *            {@link SMRestaurant} tipinde JSON
	 * @return {@link SMOperationStatus}
	 */
	@RequestMapping(path = RESTAURANT_PATH, method = RequestMethod.POST)
	public SMOperationStatus addRestaurantWithTranslation(@RequestBody SMRestaurant reqRestaurant) {
		logger.debug("Restaurant name: {}", reqRestaurant.getName());

		// restoran için seçilen varsayılan dili repository'den çekmek için
		// kullanılır
		Language selectedLanguage = langRepo.findByCode(reqRestaurant.getDefaultLangCode());

		Restaurant restaurant = new Restaurant();
		restaurant.setName(reqRestaurant.getName());
		restaurant.setLanguage(selectedLanguage);
		restaurant.setRestaurantTranslations(new ArrayList<>());

		// tüm dil seçeneklerini alıp ekle
		for (SMRestaurantLang restLanguages : reqRestaurant.getTranslations()) {
			RestaurantTranslation restaurantTranslation = new RestaurantTranslation();
			restaurantTranslation.setInformation(restLanguages.getInformation());
			restaurantTranslation.setLanguage(langRepo.findByCode(restLanguages.getLangCode()));
			restaurantTranslation.setRestaurant(restaurant);

			restaurant.getRestaurantTranslations().add(restaurantTranslation);
		}

		restaurant = restaurantRepo.saveAndFlush(restaurant);

		SMOperationStatus responseItem = new SMOperationStatus();
		responseItem.setRecordId(restaurant.getId());
		responseItem.setIsSuccessful(true);

		return responseItem;
	}

	@RequestMapping(path = RESTAURANT_PATH_WITH_ID, method = RequestMethod.GET)
	public SMRestaurant getRestaurantWithID(@PathVariable(RESTAURANT_ID) Long restaurantId) {
		logger.debug("Restaurant id: {}", restaurantId);

		SMRestaurant temp;
		Restaurant res = restaurantRepo.findOne(restaurantId);

		temp = new SMRestaurant();
		temp.setDefaultLangCode(res.getLanguage().getCode());
		temp.setId(res.getId().toString());
		temp.setName(res.getName());
		temp.setTranslations(new ArrayList<>());
		temp.setCreatedAt(res.getCreatedAt().toString());
		temp.setUpdatedAt(res.getUpdatedAt().toString());

		for (RestaurantTranslation resTrans : res.getRestaurantTranslations()) {
			SMRestaurantLang restLang = new SMRestaurantLang();
			restLang.setInformation(resTrans.getInformation());
			restLang.setLangCode(resTrans.getLanguage().getCode());
			temp.getTranslations().add(restLang);
		}

		return temp;
	}

	/**
	 * Bir restoranın dile bağlı olmayan özelliklerini güncellemek için
	 * kullanılır.
	 * 
	 * @param restaurantId
	 * @param reqRestaurant
	 * @return {@link SMOperationStatus}
	 */
	@RequestMapping(path = RESTAURANT_PATH_WITH_ID, method = RequestMethod.POST)
	public SMOperationStatus updateRestaurant(@PathVariable(RESTAURANT_ID) Long restaurantId,
			@RequestBody SMRestaurant reqRestaurant) {
		logger.debug("Restaurant id: {}", restaurantId);

		Restaurant restaurantToUpdate = restaurantRepo.findOne(restaurantId);
		restaurantToUpdate.setName(reqRestaurant.getName());
		restaurantToUpdate.setLanguage(langRepo.findByCode(reqRestaurant.getDefaultLangCode()));

		// sadece restorana ait özellikleri güncellenir
		restaurantToUpdate = restaurantRepo.saveAndFlush(restaurantToUpdate);

		SMOperationStatus status = new SMOperationStatus();
		status.setIsSuccessful(true);
		status.setRecordId(new Long(restaurantToUpdate.getId()));

		return status;
	}

	/**
	 * Restorana yeni bir dil tercümesi eklemek için kullanılır. Eğer eklemek
	 * istenen dil mevcutsa güncellenir yoksa eklenir.
	 * 
	 * @param restaurantId
	 * @param reqRestaurantLanguages
	 * @return {@link SMOperationStatus}
	 */
	@RequestMapping(path = RESTAURANT_PATH_WITH_ID + "/translations", method = RequestMethod.POST)
	public SMOperationStatus addRestaurantTranslation(@PathVariable(RESTAURANT_ID) Long restaurantId,
			@RequestBody List<SMRestaurantLang> reqRestaurantLanguages) {
		logger.debug("Add translations to Restaurant id: {}", restaurantId);

		// gelen tüm dilleri map e atıyorum sonraki işlemlerde kolaylık için
		Map<String, SMRestaurantLang> mapLangReq = new HashMap<>();
		for (SMRestaurantLang restaurantLang : reqRestaurantLanguages) {
			mapLangReq.put(restaurantLang.getLangCode(), restaurantLang);
		}

		Restaurant restaurantToUpdateTranslation = restaurantRepo.findOne(restaurantId);

		for (RestaurantTranslation restaurantTranslation : restaurantToUpdateTranslation.getRestaurantTranslations()) {
			SMRestaurantLang currentLang = mapLangReq.get(restaurantTranslation.getLanguage().getCode());

			// elimizde olan bir dilse güncellenir
			if (currentLang != null) {
				restaurantTranslation.setInformation(currentLang.getInformation());
				mapLangReq.remove(currentLang.getLangCode());
			}
		}

		// elimizde olmayan ama bize gelen diller varsa onları da ekleyelim
		for (SMRestaurantLang restaurantLang : mapLangReq.values()) {

			RestaurantTranslation restaurantTranslation = new RestaurantTranslation();
			restaurantTranslation.setInformation(restaurantLang.getInformation());
			restaurantTranslation.setLanguage(langRepo.findByCode(restaurantLang.getLangCode()));
			restaurantTranslation.setRestaurant(restaurantToUpdateTranslation);

			restaurantToUpdateTranslation.getRestaurantTranslations().add(restaurantTranslation);
		}

		restaurantRepo.saveAndFlush(restaurantToUpdateTranslation);

		SMOperationStatus status = new SMOperationStatus();
		status.setIsSuccessful(true);
		status.setRecordId(restaurantToUpdateTranslation.getId());

		return status;
	}

	/**
	 * Seçilen bir restoran için, istenilen bir dili çıkarmaya yarar.
	 * 
	 * @param restaurantId:
	 *            İşlem yapılacak restoran
	 * @param languageCode:
	 *            Kaldırılacak dil tercihi
	 * @return {@link SMOperationStatus}
	 */
	@RequestMapping(path = RESTAURANT_PATH_WITH_ID + "/translations/remove/{" + RootContexts.LANGUAGE_CODE
			+ "}", method = RequestMethod.POST)
	public SMOperationStatus removeRestaurantTranslation(@PathVariable(RESTAURANT_ID) Long restaurantId,
			@PathVariable(RootContexts.LANGUAGE_CODE) String languageCode) {

		SMOperationStatus status = new SMOperationStatus();
		status.setIsSuccessful(false);

		Restaurant restaurant = restaurantRepo.getOne(restaurantId);

		// veri modellerimizin cascede tipi ayarları ilişkisel verilerin
		// öncelikle parent bileşenlerden kaldırılmasını zorluyor. Bu yüzden
		// önce restoranın içinden istenilen dil tercihi kaldırıalcak.
		for (RestaurantTranslation resTrans : restaurant.getRestaurantTranslations()) {
			if (resTrans.getLanguage().getCode().equals(languageCode)) {

				// önce restoranın listesinden çıkaralım
				restaurant.getRestaurantTranslations().remove(resTrans);

				// şimdi de çıkardığımız bileşeni repositorye bildirelim
				restaurantTranslationRepo.delete(resTrans);

				status.setIsSuccessful(true);
				break;
			}
		}

		return status;
	}

	@RequestMapping(path = RESTAURANT_PATH_WITH_ID + "/remove", method = RequestMethod.POST)
	public SMOperationStatus deleteRestaurant(@PathVariable(RESTAURANT_ID) Long restaurantId) {
		SMOperationStatus operationStatus = new SMOperationStatus();

		restaurantRepo.delete(restaurantId);
		operationStatus.setIsSuccessful(true);
		return operationStatus;
	}

	@RequestMapping(path = RESTAURANT_PATH_WITH_ID + "/alldetails", method = RequestMethod.GET)
	public SMRestaurant getEveryDetailsOfRestaurantWithID(@PathVariable(RESTAURANT_ID) Long restaurantId) {
		logger.debug("Restaurant id: {}", restaurantId);

		SMRestaurant responseRestaurant;
		Restaurant res = restaurantRepo.findOne(restaurantId);

		responseRestaurant = new SMRestaurant();
		responseRestaurant.setDefaultLangCode(res.getLanguage().getCode());
		responseRestaurant.setId(res.getId().toString());
		responseRestaurant.setCreatedAt(res.getCreatedAt().toString());
		responseRestaurant.setUpdatedAt(res.getUpdatedAt().toString());
		responseRestaurant.setName(res.getName());
		responseRestaurant.setTranslations(new ArrayList<SMRestaurantLang>());
		responseRestaurant.setMenu(new ArrayList<SMMenu>());

		SMRestaurantCategory responseRestaurantCategory = new SMRestaurantCategory();
		responseRestaurantCategory.setGenericname(res.getRestaurantCategory().getCode());
		responseRestaurantCategory.setTranslations(new ArrayList<>());

		for (RestaurantCategoryTranslation restaurantCategoryTranslation : res.getRestaurantCategory()
				.getRestaurantCategoryTranslations()) {
			SMRestaurantCategoryLang responseRestaurantCategoryLang = new SMRestaurantCategoryLang();
			responseRestaurantCategoryLang.setTranslatedname(restaurantCategoryTranslation.getName());
			responseRestaurantCategoryLang.setLangCode(restaurantCategoryTranslation.getLanguage().getCode());
			responseRestaurantCategoryLang.setDescription(restaurantCategoryTranslation.getDescription());
			responseRestaurantCategoryLang.setUpdatedAt(restaurantCategoryTranslation.getUpdatedAt().toString());
			responseRestaurantCategory.getTranslations().add(responseRestaurantCategoryLang);
		}

		responseRestaurant.setRestaurantCategory(responseRestaurantCategory);

		for (RestaurantTranslation resTrans : res.getRestaurantTranslations()) {
			SMRestaurantLang restLang = new SMRestaurantLang();
			restLang.setInformation(resTrans.getInformation());
			restLang.setLangCode(resTrans.getLanguage().getCode());
			responseRestaurant.getTranslations().add(restLang);
		}

		for (Menu menu : res.getMenus()) {

			logger.debug("Menu ID In Restaurant: {}", menu.getId());
			SMMenu responseMenu = new SMMenu();
			responseMenu.setGenericname(menu.getGenericName());
			responseMenu.setId(menu.getId().toString());
			responseMenu.setSpecialOffer(menu.getSpecialOffer());
			responseMenu.setParentMenuId(menu.getParentMenuId());
			responseMenu.setCreatedAt(menu.getCreatedAt().toString());
			responseMenu.setUpdatedAt(menu.getUpdatedAt().toString());
			responseMenu.setMenuTranslations(new ArrayList<SMMenuLang>());
			responseMenu.setItemsInMenus(new ArrayList<SMItemsInMenu>());

			// translation array

			for (MenuTranslation menuTranslation : menu.getMenuTranslations()) {

				SMMenuLang responseMenuTranslation = new SMMenuLang();
				responseMenuTranslation.setDescription(menuTranslation.getDescription());
				responseMenuTranslation.setLangCode(menuTranslation.getLanguage().getCode());
				responseMenuTranslation.setTitle(menuTranslation.getName());
				responseMenuTranslation.setUpdatedAt(menuTranslation.getUpdatedAt().toString());
				responseMenu.getMenuTranslations().add(responseMenuTranslation);
			}

			for (ItemsInMenus item : itemsInMenusRepo.findMenuItemsByMenuId(menu.getId())) {

				SMItemsInMenu responseItem = new SMItemsInMenu();
				responseItem.setItemDiscountRate(item.getItemDiscountRate());
				responseItem.setItemPrice(item.getItemPrice());
				responseItem.setMenuItem(item.getMenuItemId());
				responseItem.setUpdatedAt(item.getUpdatedAt().toString());

				SMMenuProduct responseMenuItem = new SMMenuProduct();
				logger.debug("Menu Items ID In Menu: {}", item.getMenuItemId());

				MenuItem menuItem = menuItemRepo.findOne(item.getMenuItemId());

				responseMenuItem.setCalori(menuItem.getCalori());
				responseMenuItem.setCurrencyCode(menuItem.getCurrencyBean().getCode());
				responseMenuItem.setIsHelal(menuItem.getIsHelal());
				responseMenuItem.setListOrder(menuItem.getListOrder());
				responseMenuItem.setPrice(menuItem.getPrice());
				responseMenuItem.setServiceTime(menuItem.getServiceTime());
				responseMenuItem.setCreatedAt(menuItem.getCreatedAt().toString());
				responseMenuItem.setUpdatedAt(menuItem.getUpdatedAt().toString());

				responseMenuItem.setGroupId(menuItem.getMenuGroup().getId().toString());
				responseMenuItem.setProductTranslations(new ArrayList<>());
				responseMenuItem.setIngredients(new ArrayList<>());

				for (MenuItemTranslation menuItemTranslation : menuItem.getMenuItemTranslations()) {

					SMMenuProductLang responseMenuItemTranslation = new SMMenuProductLang();
					responseMenuItemTranslation.setDescription(menuItemTranslation.getDescription());
					responseMenuItemTranslation.setLangCode(menuItemTranslation.getLanguage().getCode());
					responseMenuItemTranslation.setName(menuItemTranslation.getName());
					responseMenuItemTranslation.setUpdatedAt(menuItemTranslation.getUpdatedAt().toString());
					responseMenuItem.getProductTranslations().add(responseMenuItemTranslation);

				}

				for (MenuItemIngredient menuItemIngredient : menuItemIngredientRepo
						.findIngredientsByMenuItemId(menuItem.getId())) {
					responseMenuItem.getIngredients().add(menuItemIngredient.getIngredientId());

				}
				responseItem.setMenuItemAsMenuItem(responseMenuItem);

				responseMenu.getItemsInMenus().add(responseItem);
			}

			responseRestaurant.getMenu().add(responseMenu);
		}

		return responseRestaurant;
	}

}
