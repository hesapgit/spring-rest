package com.zill.service.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zill.data.dao.LanguageRepository;
import com.zill.data.dao.MenuGroupRepository;
import com.zill.data.dao.MenuItemRepository;
import com.zill.data.dao.RestaurantRepository;
import com.zill.data.model.MenuGroup;
import com.zill.data.model.MenuGroupTranslation;
import com.zill.service.model.SMMenuGroup;
import com.zill.service.model.SMMenuGroupLang;
import com.zill.service.model.SMOperationStatus;
import com.zill.util.RootContexts;

@RestController
@RequestMapping(path = RootContexts.MENU_GROUP_ROOT)
@Consumes("application/json; charset=UTF-8")
@Produces("application/json; charset=UTF-8")
public class MenuGroupController {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final String MENU_GROUP_ID = "groupId";

	private static final String MENU_GROUP_ID_PATH = "/{" + MENU_GROUP_ID + "}";

	private static final String RESTAURANT_ID = "resturantId";

	private static final String MENU_GROUP_RESTAURANT = "/resturant/{" + RESTAURANT_ID + "}";

	@Autowired
	private MenuGroupRepository menuGroupRepo;

	@Autowired
	private RestaurantRepository restRepo;

	@Autowired
	private MenuItemRepository menuItemRepo;

	@Autowired
	private LanguageRepository languageRepo;

	// create new menuGroup
	@RequestMapping(method = RequestMethod.POST)
	public SMOperationStatus addMenuGroup(@RequestBody SMMenuGroup groupToAdd) {
		SMOperationStatus status = new SMOperationStatus();

		MenuGroup newGroup = new MenuGroup();
		newGroup.setCode(groupToAdd.getCode());
		newGroup.setMenuGroupTranslations(new ArrayList<>());

		for (SMMenuGroupLang groupLang : groupToAdd.getGroupTranslations()) {
			MenuGroupTranslation groupTranslation = new MenuGroupTranslation();
			groupTranslation.setLanguage(languageRepo.findByCode(groupLang.getLangCode()));
			groupTranslation.setName(groupLang.getName());
			groupTranslation.setDescription(groupLang.getDescription());

			newGroup.addMenuGroupTranslation(groupTranslation);
		}

		newGroup = menuGroupRepo.saveAndFlush(newGroup);

		status.setIsSuccessful(true);
		status.setRecordId(newGroup.getId());

		return status;
	}

	// get All Menu Group
	@RequestMapping(method = RequestMethod.GET)
	public List<SMMenuGroup> getAllMenuGroups() {
		List<SMMenuGroup> responseContent = new ArrayList<>();
		List<MenuGroup> distintMenuGroup = menuGroupRepo.findAll();

		for (MenuGroup oneMenuGroup : distintMenuGroup) {
			SMMenuGroup response = new SMMenuGroup();
			response.setId(oneMenuGroup.getId().toString());
			response.setCode(oneMenuGroup.getCode());
			response.setGroupTranslations(new ArrayList<>());

			for (MenuGroupTranslation groupTranslation : oneMenuGroup.getMenuGroupTranslations()) {
				SMMenuGroupLang groupLang = new SMMenuGroupLang();
				groupLang.setDescription(groupTranslation.getDescription());
				groupLang.setLangCode(groupTranslation.getLanguage().getCode());
				groupLang.setName(groupTranslation.getName());

				response.getGroupTranslations().add(groupLang);
			}

			responseContent.add(response);
		}

		return responseContent;
	}
	// tekrar bak

	@RequestMapping(path = MENU_GROUP_ID_PATH + "/update", method = RequestMethod.POST)
	public SMOperationStatus updateMenuGroup(@PathVariable(MENU_GROUP_ID) Long menugroupId,
			@RequestBody SMMenuGroup reqMenuGroup) {

		SMOperationStatus operationStatus = new SMOperationStatus();

		logger.debug("menuGroup id: {}", menugroupId);

		MenuGroup menuGroupUptoDate = menuGroupRepo.getOne(menugroupId);

		if (menuGroupUptoDate == null) {
			operationStatus.setIsSuccessful(false);
			operationStatus.setMessage("Requested MenuGroup can not found!!!");
			return operationStatus;
		}

		menuGroupUptoDate.setCode(reqMenuGroup.getCode());

		Map<String, SMMenuGroupLang> mapLangTranslation = new HashMap<>();

		for (SMMenuGroupLang groupLang : reqMenuGroup.getGroupTranslations()) {
			mapLangTranslation.put(groupLang.getLangCode(), groupLang);
		}

		for (MenuGroupTranslation itemTranslation : menuGroupUptoDate.getMenuGroupTranslations()) {
			SMMenuGroupLang groupLang = mapLangTranslation.get(itemTranslation.getLanguage().getCode());
			if (groupLang != null) {
				itemTranslation.setDescription(groupLang.getDescription());
				itemTranslation.setName(groupLang.getName());
				mapLangTranslation.remove(groupLang.getLangCode());
			}
		}
		for (SMMenuGroupLang groupMenuLang : mapLangTranslation.values()) {
			MenuGroupTranslation itemTranslation = new MenuGroupTranslation();
			itemTranslation.setLanguage(languageRepo.getOne(groupMenuLang.getLangCode()));
			itemTranslation.setDescription(groupMenuLang.getDescription());
			itemTranslation.setName(groupMenuLang.getName());
			itemTranslation.setMenuGroup(menuGroupUptoDate);

			menuGroupUptoDate.getMenuGroupTranslations().add(itemTranslation);
		}

		menuGroupUptoDate = menuGroupRepo.saveAndFlush(menuGroupUptoDate);
		operationStatus.setIsSuccessful(true);
		operationStatus.setRecordId(menuGroupUptoDate.getId());
		return operationStatus;

	}

	// tekrar bak
	@RequestMapping(path = MENU_GROUP_RESTAURANT, method = RequestMethod.GET)
	public List<SMMenuGroup> getAllMenuGroupsofRestaurant(@PathVariable(RESTAURANT_ID) Long restaurantId) {
		List<SMMenuGroup> responseContent = new ArrayList<>();
		List<MenuGroup> distintMenuGroup = menuItemRepo.findDistinctByRestaurantId(restRepo.findOne(restaurantId));

		for (MenuGroup menuGroup : distintMenuGroup) {
			MenuGroup temp = new MenuGroup();
			temp = menuGroupRepo.findOne(menuGroup.getId());

			SMMenuGroup response = new SMMenuGroup();
			response.setCode(temp.getCode());
			response.setGroupTranslations(new ArrayList<>());
			response.setId(temp.getId().toString());

			for (MenuGroupTranslation groupTranslation : temp.getMenuGroupTranslations()) {
				SMMenuGroupLang groupLang = new SMMenuGroupLang();
				groupLang.setDescription(groupTranslation.getDescription());
				groupLang.setLangCode(groupTranslation.getLanguage().getCode());
				groupLang.setName(groupTranslation.getName());

				response.getGroupTranslations().add(groupLang);
			}

			// add the item to response list
			responseContent.add(response);
		}

		return responseContent;
	}

	// @RequestMapping(path = MENU_GROUP_PATH, method = RequestMethod.GET)
	// public List<SMMenuGroup>
	// getAllMenuGroups(@PathVariable(RootContexts.RESTAURANT_ID) Long
	// restaurantId) {
	//
	// logger.debug("Get all categories requested for Restaurant: {}",
	// restaurantId);
	// List<SMMenuGroup> responseContent = new ArrayList<>();
	// List <MenuItem> menuItems =
	// menuItemRepo.findByRestaurantId(restaurantId);
	// List<MenuGroup> distintMenuGroup = new ArrayList<>();
	// List <MenuGroup> menugro = menuGroupRepo.findAll();
	//
	//
	//
	//
	//
	// for(MenuItem menuIt: menuItems){
	// distintMenuGroup.add(menuIt.getMenuGroup());
	// }
	// // add elements all, including duplicates
	// Set<MenuGroup> hs = new HashSet<>();
	// hs.addAll(distintMenuGroup);
	// distintMenuGroup.clear();
	// distintMenuGroup.addAll(hs);
	//
	//
	//// for(MenuItem menuIt: menuItems){
	////
	////
	//// SMMenuGroup response = new SMMenuGroup();
	//// response.setCode(menuGroup.getCode());
	//// response.setGroupTranslations(new ArrayList<>());
	//// response.setId(menuGroup.getId().toString());
	////
	//// for (MenuGroupTranslation groupTranslation :
	// menuGroup.getMenuGroupTranslations()){
	////
	//// SMMenuGroupLang groupLang = new SMMenuGroupLang();
	//// groupLang.setDescription(groupTranslation.getDescription());
	//// groupLang.setLangCode(groupTranslation.getLanguage().getCode());
	//// groupLang.setName(groupTranslation.getName());
	////
	//// response.getGroupTranslations().add(groupLang);
	////
	////
	//// }
	//// responseContent.add(response);
	//// }
	//
	// for (MenuGroup menuGroup : distintMenuGroup) {
	// SMMenuGroup response = new SMMenuGroup();
	// response.setCode(menuGroup.getCode());
	// response.setGroupTranslations(new ArrayList<>());
	// response.setId(menuGroup.getId().toString());
	//
	// for (MenuGroupTranslation groupTranslation :
	// menuGroup.getMenuGroupTranslations()) {
	// SMMenuGroupLang groupLang = new SMMenuGroupLang();
	// groupLang.setDescription(groupTranslation.getDescription());
	// groupLang.setLangCode(groupTranslation.getLanguage().getCode());
	// groupLang.setName(groupTranslation.getName());
	//
	// response.getGroupTranslations().add(groupLang);
	// }
	//
	// // add the item to response list
	// responseContent.add(response);
	// }
	//
	// return responseContent;
	// }

	// delete MenuGroup begin
	@RequestMapping(path = MENU_GROUP_ID_PATH + "/remove", method = RequestMethod.POST)
	public SMOperationStatus deleteMenuGroup(@PathVariable(MENU_GROUP_ID) Long menugroupId) {

		SMOperationStatus operationStatus = new SMOperationStatus();

		menuGroupRepo.delete(menugroupId);
		operationStatus.setIsSuccessful(true);
		return operationStatus;

	}
	// delete MenuGroup ends
	/*
	 * public SMOperationStatus
	 * deleteMenuGroup(@PathVariable(RootContexts.RESTAURANT_ID) String
	 * restaurantId,
	 * 
	 * @PathVariable(MENU_GROUP_ID) String menuGroupCode) { SMOperationStatus
	 * operationStatus = new SMOperationStatus();
	 * 
	 * // delete with ID MenuGroup menuGroupToDelete =
	 * menuGroupRepo.findByCode(menuGroupCode);
	 * 
	 * if (menuGroupToDelete != null) { menuGroupRepo.delete(menuGroupToDelete);
	 * operationStatus.setIsSuccessful(true); } else {
	 * operationStatus.setIsSuccessful(false); }
	 * 
	 * return operationStatus; }
	 */

}
