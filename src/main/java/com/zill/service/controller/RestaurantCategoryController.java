package com.zill.service.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zill.data.dao.LanguageRepository;
import com.zill.data.dao.RestaurantCategoryRepository;
import com.zill.data.model.RestaurantCategory;
import com.zill.data.model.RestaurantCategoryTranslation;
import com.zill.service.model.SMOperationStatus;
import com.zill.service.model.SMRestaurantCategory;
import com.zill.service.model.SMRestaurantCategoryLang;
import com.zill.util.RootContexts;

@RestController
@RequestMapping(path = RootContexts.RESTAURANT_GROUP_ROOT)
@Consumes("application/json; charset=UTF-8")
@Produces("application/json; charset=UTF-8")
public class RestaurantCategoryController {

	private static final String RESTAURANT_GROUP_ID = "restaurantGroupId";
	private static final String RESTAURANT_GROUP_ID_PATH = "/{" + RESTAURANT_GROUP_ID + "}";
	private static final String RESTAURANT_GROUP_LANGCODE = "/{" + RootContexts.LANGUAGE_CODE + "}";

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private RestaurantCategoryRepository restaurantCategoryRepo;

	@Autowired
	private LanguageRepository langRepo;

	@RequestMapping(method = RequestMethod.GET)
	public List<SMRestaurantCategory> getAllResturantCategories() {
		logger.debug("Get all available languages requested for ResturantCategory");
		List<SMRestaurantCategory> responseContent = new ArrayList<>();

		SMRestaurantCategory temp;
		for (RestaurantCategory restCat : restaurantCategoryRepo.findAll()) {
			temp = new SMRestaurantCategory();
			temp.setId(restCat.getId().toString());
			temp.setGenericname(restCat.getCode());
			temp.setTranslations(new ArrayList<>());
			if (restCat.getRestaurantCategoryTranslations().size() > 0) {
				for (RestaurantCategoryTranslation restCatTrans : restCat.getRestaurantCategoryTranslations()) {
					SMRestaurantCategoryLang restCatLang = new SMRestaurantCategoryLang();
					restCatLang.setTranslatedname(restCatTrans.getName());
					restCatLang.setLangCode(restCatTrans.getLanguage().getCode());
					restCatLang.setDescription(restCatTrans.getDescription());
					temp.getTranslations().add(restCatLang);
				}
			}
			responseContent.add(temp);
		}

		return responseContent;
	}

	@RequestMapping(path = RESTAURANT_GROUP_LANGCODE, method = RequestMethod.GET)
	public List<SMRestaurantCategory> getRestaurantCategorysByLangCode(
			@PathVariable(RootContexts.LANGUAGE_CODE) String languageCode) {

		logger.debug("Get all available languages requested for Resturant Group for Lang Code: {}", languageCode);

		List<SMRestaurantCategory> allRestaurantCategories = new ArrayList<>();

		SMRestaurantCategory temp;
		for (RestaurantCategory restCat : restaurantCategoryRepo.findAll()) {

			temp = new SMRestaurantCategory();
			temp.setId(restCat.getId().toString());
			temp.setGenericname(restCat.getCode());
			temp.setTranslations(new ArrayList<>());
			if (restCat.getRestaurantCategoryTranslations().size() > 0) {
				for (RestaurantCategoryTranslation restCatTrans : restCat.getRestaurantCategoryTranslations()) {
					if (restCatTrans.getLanguage().getCode().equals(languageCode)) {
						SMRestaurantCategoryLang restCatLang = new SMRestaurantCategoryLang();
						restCatLang.setTranslatedname(restCatTrans.getName());
						restCatLang.setLangCode(languageCode);
						restCatLang.setDescription(restCatTrans.getDescription());
						temp.getTranslations().add(restCatLang);

					}
				}
			}
			allRestaurantCategories.add(temp);
		}
		return allRestaurantCategories;
	}

	@RequestMapping(method = RequestMethod.POST)
	public SMOperationStatus addRestaurantCategoryWithTranslation(
			@RequestBody SMRestaurantCategory reqRestaurantCategory) {
		logger.debug("RestaurantCategory name: {}", reqRestaurantCategory.getGenericname());

		RestaurantCategory restaurantCategory = new RestaurantCategory();
		restaurantCategory.setCode(reqRestaurantCategory.getGenericname());
		restaurantCategory.setRestaurantCategoryTranslations(new ArrayList<>());

		for (SMRestaurantCategoryLang resCatLanguages : reqRestaurantCategory.getTranslations()) {
			RestaurantCategoryTranslation restaurantCategoryTranslation = new RestaurantCategoryTranslation();
			restaurantCategoryTranslation.setName(resCatLanguages.getTranslatedname());
			restaurantCategoryTranslation.setLanguage(langRepo.findByCode(resCatLanguages.getLangCode()));
			restaurantCategoryTranslation.setDescription(resCatLanguages.getDescription());
			restaurantCategoryTranslation.setRestaurantCategory(restaurantCategory);

			restaurantCategory.getRestaurantCategoryTranslations().add(restaurantCategoryTranslation);
		}
		restaurantCategory = restaurantCategoryRepo.saveAndFlush(restaurantCategory);

		SMOperationStatus responseItem = new SMOperationStatus();
		responseItem.setRecordId(restaurantCategory.getId());
		responseItem.setIsSuccessful(true);

		return responseItem;
	}

	// delete ingredients
	@RequestMapping(path = RESTAURANT_GROUP_ID_PATH + "/remove", method = RequestMethod.POST)
	public SMOperationStatus deleteIngredient(@PathVariable(RESTAURANT_GROUP_ID) Long resturantGrouptId) {
		SMOperationStatus operationStatus = new SMOperationStatus();
		restaurantCategoryRepo.delete(resturantGrouptId);
		operationStatus.setIsSuccessful(true);
		return operationStatus;
	}

}
