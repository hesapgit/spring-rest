package com.zill.service.controller;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.zill.data.dao.ImageDetailRepository;
import com.zill.data.dao.MenuImageRepository;
import com.zill.data.dao.MenuItemImageRepository;
import com.zill.data.dao.MenuItemRepository;
import com.zill.data.dao.MenuRepository;
import com.zill.data.dao.RestaurantImageRepository;
import com.zill.data.dao.RestaurantRepository;
import com.zill.data.model.ImageDetail;
import com.zill.data.model.Menu;
import com.zill.data.model.MenuImage;
import com.zill.data.model.MenuItem;
import com.zill.data.model.MenuItemImage;
import com.zill.data.model.Restaurant;
import com.zill.data.model.RestaurantImage;
import com.zill.service.model.SMMenuImage;
import com.zill.service.model.SMMenuItemImage;
import com.zill.service.model.SMOperationStatus;
import com.zill.service.model.SMRestaurantImage;
import com.zill.util.RootContexts;

@RestController
@Consumes("application/json; charset=UTF-8")
@Produces("application/json; charset=UTF-8")
@RequestMapping(path = RootContexts.IMAGE_ROOT_PATH)
public class ImageController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private MenuImageRepository menuImageRepo;
	@Autowired
	private MenuRepository menuRepo;
	@Autowired
	private MenuItemImageRepository menuItemImageRepo;

	@Autowired
	private RestaurantRepository restaurantRepo;
	@Autowired
	private MenuItemRepository menuItemRepo;
	@Autowired
	private RestaurantImageRepository restaurantImageRepo;

	@Autowired
	private ImageDetailRepository imageDetailRepo;

	public static final String REFERENCED_IMAGE_PATH = RootContexts.IMAGE_PATH + "/{" + RootContexts.RESTAURANT_ID
			+ "}";
	public static final String RESTAURANT_REFERENCED_IMAGE_PATH = "/{" + RootContexts.REFERENCE_IMAGE_ID + "}";
	public static final String RESTAURANT_IMAGE_INFO_PATH = "/info/{" + RootContexts.REFERENCE_IMAGE_ID + "}";
	public static final String RESTAURANT_UPDATE_IMAGE_INFO_PATH = "/updateinfo/{" + RootContexts.REFERENCE_IMAGE_ID
			+ "}";
	public static final String RESTAURANT_IMAGE_UPLOAD_PATH = "/upload";
	public static final String RESTAURANT_IMAGE_DELETE_PATH = "/delete/{" + RootContexts.REFERENCE_IMAGE_ID + "}";

	public static final String MENU_ITEM_IMAGE_PATH = "/menuitem";
	public static final String MENU_ITEM_ID_IMAGE_PATH = MENU_ITEM_IMAGE_PATH + "/{" + RootContexts.MENU_ITEM_ID + "}";
	public static final String MENU_ITEM_REFERENCED_IMAGE_PATH = MENU_ITEM_IMAGE_PATH + "/{"
			+ RootContexts.REFERENCE_IMAGE_ID + "}";
	public static final String MENU_ITEM_IMAGE_INFO_PATH = MENU_ITEM_IMAGE_PATH + "/info/{"
			+ RootContexts.REFERENCE_IMAGE_ID + "}";
	public static final String MENU_ITEM_IMAGE_UPLOAD_PATH = MENU_ITEM_ID_IMAGE_PATH + "/upload";
	public static final String MENU_ITEM_IMAGE_DELETE_PATH = MENU_ITEM_IMAGE_PATH + "/delete/{"
			+ RootContexts.REFERENCE_IMAGE_ID + "}";
	public static final String MENU_ITEM_UPDATE_IMAGE_INFO_PATH = MENU_ITEM_IMAGE_PATH + "/updateinfo/{"
			+ RootContexts.REFERENCE_IMAGE_ID + "}";

	public static final String MENU_IMAGE_PATH = "/menu";
	public static final String MENU_ID_IMAGE_PATH = MENU_IMAGE_PATH + "/{" + RootContexts.MENU_ID + "}";
	public static final String MENU_REFERENCED_IMAGE_PATH = MENU_IMAGE_PATH + "/{" + RootContexts.REFERENCE_IMAGE_ID
			+ "}";
	public static final String MENU_IMAGE_INFO_PATH = MENU_IMAGE_PATH + "/info/{" + RootContexts.REFERENCE_IMAGE_ID
			+ "}";
	public static final String MENU_IMAGE_UPLOAD_PATH = MENU_ID_IMAGE_PATH + "/upload";
	public static final String MENU_IMAGE_DELETE_PATH = MENU_IMAGE_PATH + "/delete/{" + RootContexts.REFERENCE_IMAGE_ID
			+ "}";
	public static final String MENU_UPDATE_IMAGE_INFO_PATH = MENU_IMAGE_PATH + "/updateinfo/{"
			+ RootContexts.REFERENCE_IMAGE_ID + "}";

	// createImageInfo
	// getImageInfo
	// getImage
	// upload Image
	// delete Image
	// update ImageInfo

	// --------------Restaurant Image

	@RequestMapping(path = RESTAURANT_IMAGE_INFO_PATH, method = RequestMethod.GET)
	public SMRestaurantImage getRestaurantImageInfo(
			@PathVariable(RootContexts.REFERENCE_IMAGE_ID) String referenceImageId) {

		ImageDetail requestImage = new ImageDetail();
		SMRestaurantImage response = new SMRestaurantImage();

		requestImage = imageDetailRepo.findByReferenceImagePath(referenceImageId);
		RestaurantImage restImage = restaurantImageRepo.findByImageDetail(requestImage);

		logger.debug("referencePath for Image: {}", requestImage.getReferenceImagePath());

		response.setId(requestImage.getId().toString());
		response.setImagePath(requestImage.getImagePath());
		response.setDescription(requestImage.getDescription());
		response.setSize(requestImage.getSize());
		response.setReferenceImagePath(requestImage.getReferenceImagePath());
		response.setRestaurant(restImage.getRestaurant().getId());
		response.setUpdatedAt(requestImage.getUpdatedAt().toString());

		return response;
	}

	@Produces("image/*")
	@RequestMapping(path = RESTAURANT_REFERENCED_IMAGE_PATH, method = RequestMethod.GET)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public HttpEntity getRestaurantImage(HttpServletResponse response,
			@PathVariable(RootContexts.RESTAURANT_ID) String restaurantId,
			@PathVariable(RootContexts.REFERENCE_IMAGE_ID) String referenceImageId) throws IOException {

		ImageDetail responseItem = imageDetailRepo.findByReferenceImagePath(referenceImageId);
		logger.debug("Get url requested for ImageId: {}", responseItem.getReferenceImagePath());

		String url = new String(RootContexts.SERVER_IMAGE_PATH + "/" + restaurantId + "/" + "restaurant" + "/"
				+ responseItem.getImagePath());
		logger.debug("Get image requested for url: {}", url);

		InputStream imageIS = new BufferedInputStream(new FileInputStream(url));

		BufferedImage image = ImageIO.read(imageIS);

		// Base64.encodeBase64(imageData);

		try (InputStream inputStream = imageIS) {
			OutputStream out = response.getOutputStream();

			ImageIO.write(image, FilenameUtils.getExtension(url), out);

		} catch (IOException e) {
			// handle
		}
		return new ResponseEntity<Image>(HttpStatus.OK);

		// final InputStream bigInputStream =
		// new ByteArrayInputStream();

		// return Response.ok(imageData).build();
	}
	// file upload----------------

	@Consumes("image/*")
	@RequestMapping(path = RESTAURANT_IMAGE_UPLOAD_PATH, method = RequestMethod.POST)
	public @ResponseStatus SMOperationStatus uploadRestaurantImageHandler(
			@PathVariable(RootContexts.RESTAURANT_ID) Long restaurantId, @RequestParam("image") MultipartFile file,
			@RequestParam(value = "description", required = false) String description,
			@RequestParam(value = "size", required = false) Integer size) {
		SMOperationStatus status = new SMOperationStatus();

		String name = file.getOriginalFilename();

		logger.debug("Get type requested for image: {}", file.getContentType());

		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
				String rootPath = RootContexts.SERVER_IMAGE_PATH + File.separator + restaurantId.toString()
						+ File.separator + "restaurant";

				logger.debug("Get path requested for image: {}", rootPath);
				File dir = new File(rootPath);
				if (!dir.exists()) {

					status.setMessage("Folder doesn't exist. Suggestions: /restaurant, /menu, /menuitem");
					status.setIsSuccessful(false);
					return status;
					// dir.mkdirs();
				}

				// fill database info
				RestaurantImage newRestImage = new RestaurantImage();
				ImageDetail newImageDetail = new ImageDetail();

				Restaurant rest = restaurantRepo.getOne(restaurantId);
				if (rest == null) {
					status.setRecordId(restaurantId);
					status.setMessage("Restaurant doesn't exist");
					status.setIsSuccessful(false);
					return status;
				}

				File serverFile = new File(dir.getAbsolutePath() + File.separator + name);

				if (serverFile.exists()) {
					name = (new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime())) + name;
					serverFile = new File(dir.getAbsolutePath() + File.separator + name);
				}
				BufferedImage bimg = ImageIO.read(file.getInputStream());

				newImageDetail.setImagePath(name);
				newImageDetail.setDescription(description);
				newImageDetail.setSize(size);
				try {
					newImageDetail = imageDetailRepo.saveAndFlush(newImageDetail);
				} catch (Exception e) {
					status.setIsSuccessful(false);
					status.setMessage("Error occured while recording to DB!");
					return status;
				}

				newRestImage.setRestaurant(rest);
				newRestImage.setImageDetail(newImageDetail);

				try {
					newRestImage = restaurantImageRepo.saveAndFlush(newRestImage);
				} catch (Exception e) {
					status.setIsSuccessful(false);
					status.setMessage("Error occured while recording to DB!");
					return status;
				}

				// Create the file on server

				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				serverFile.setReadOnly();// readonly
				stream.close();

				logger.info("Server File Location=" + serverFile.getAbsolutePath());

				status.setMessage("You successfully uploaded file=" + name + " as /restaurant/image/" + restaurantId
						+ "/" + newImageDetail.getReferenceImagePath());
				status.setIsSuccessful(true);
				status.setReferenceId(newImageDetail.getReferenceImagePath());
				status.setRecordId(newImageDetail.getId());
				return status;

			} catch (Exception e) {

				status.setMessage("You failed to upload " + name + " => " + e.getMessage());
				status.setIsSuccessful(false);
				return status;

			}
		} else {
			status.setMessage("You failed to upload " + name + " because the file was empty.");
			status.setIsSuccessful(false);
			return status;

		}
	}

	@RequestMapping(path = RESTAURANT_IMAGE_DELETE_PATH, method = RequestMethod.POST)
	public SMOperationStatus deleteRestaurantImage(@PathVariable(RootContexts.RESTAURANT_ID) String restaurantId,
			@PathVariable(RootContexts.REFERENCE_IMAGE_ID) String referenceImageId) {
		SMOperationStatus status = new SMOperationStatus();

		try {
			ImageDetail deletedItem = imageDetailRepo.findByReferenceImagePath(referenceImageId);

			if (deletedItem == null) {
				status.setIsSuccessful(false);
				status.setMessage("Requested Image can not found!!!");
				return status;
			}
			String imagePath = deletedItem.getImagePath();
			imageDetailRepo.delete(deletedItem);
			logger.debug("Get url requested for ImageId: {}", deletedItem.getId());

			String url = new String(
					RootContexts.SERVER_IMAGE_PATH + "/" + restaurantId + "/" + "restaurant" + "/" + imagePath);
			File file = new File(url);

			if (file.delete()) {

				status.setMessage(file.getName() + " is deleted!");
				status.setIsSuccessful(true);
			} else {
				System.out.println("Delete operation is failed.");
			}

		} catch (Exception e) {

			e.printStackTrace();

		}

		return status;

	}

	@RequestMapping(path = RESTAURANT_UPDATE_IMAGE_INFO_PATH, method = RequestMethod.POST)
	public SMOperationStatus updateRestaurantImageInfo(
			@PathVariable(RootContexts.REFERENCE_IMAGE_ID) String referenceImageId,
			@RequestBody SMRestaurantImage reqToUpdate) {

		SMOperationStatus status = new SMOperationStatus();

		ImageDetail imageToUpdate = imageDetailRepo.findByReferenceImagePath(referenceImageId);

		if (imageToUpdate == null) {
			status.setIsSuccessful(false);
			status.setMessage("Requested Image can not found!!!");
			return status;
		}

		Restaurant reqItem = restaurantRepo.getOne(reqToUpdate.getRestaurant());
		RestaurantImage restImage = restaurantImageRepo.findByImageDetail(imageToUpdate);
		if (reqItem == null) {
			status.setIsSuccessful(false);
			status.setMessage("Requested Product can not found!!!");
			return status;
		}
		restImage.setRestaurant(reqItem);

		try {
			restImage = restaurantImageRepo.saveAndFlush(restImage);
		} catch (Exception e) {
			status.setIsSuccessful(false);
			status.setMessage("Error occured while recording to DB!");
			return status;
		}

		imageToUpdate.setDescription(reqToUpdate.getDescription());
		imageToUpdate.setSize(reqToUpdate.getSize());

		try {
			imageToUpdate = imageDetailRepo.saveAndFlush(imageToUpdate);
		} catch (Exception e) {
			status.setIsSuccessful(false);
			status.setMessage("Error occured while recording to DB!");
			return status;
		}

		status.setIsSuccessful(true);
		status.setMessage("Item Updated");

		return status;
	}

	// --------------MenuItem Image

	@RequestMapping(path = MENU_ITEM_IMAGE_INFO_PATH, method = RequestMethod.GET)
	public SMMenuItemImage getMenuItemImageInfo(
			@PathVariable(RootContexts.REFERENCE_IMAGE_ID) String referenceImageId) {

		ImageDetail requestImage = new ImageDetail();
		SMMenuItemImage response = new SMMenuItemImage();

		requestImage = imageDetailRepo.findByReferenceImagePath(referenceImageId);
		MenuItemImage restImage = menuItemImageRepo.findByImageDetail(requestImage);

		logger.debug("referencePath for Image: {}", requestImage.getReferenceImagePath());

		response.setId(requestImage.getId().toString());
		response.setImagePath(requestImage.getImagePath());
		response.setDescription(requestImage.getDescription());
		response.setSize(requestImage.getSize());
		response.setReferenceImagePath(requestImage.getReferenceImagePath());
		response.setMenuItem(restImage.getMenuItem().getId());
		response.setUpdatedAt(requestImage.getUpdatedAt().toString());
		return response;
	}

	@Produces("image/*")
	@RequestMapping(path = MENU_ITEM_REFERENCED_IMAGE_PATH, method = RequestMethod.GET)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public HttpEntity getMenuItemImage(HttpServletResponse response,
			@PathVariable(RootContexts.RESTAURANT_ID) String restaurantId,
			@PathVariable(RootContexts.REFERENCE_IMAGE_ID) String referenceImageId) throws IOException {

		ImageDetail responseItem = imageDetailRepo.findByReferenceImagePath(referenceImageId);
		logger.debug("Get url requested for ImageId: {}", responseItem.getReferenceImagePath());

		String url = new String(RootContexts.SERVER_IMAGE_PATH + "/" + restaurantId + "/" + "menuitem" + "/"
				+ responseItem.getImagePath());
		logger.debug("Get image requested for url: {}", url);

		InputStream imageIS = new BufferedInputStream(new FileInputStream(url));

		BufferedImage image = ImageIO.read(imageIS);

		try (InputStream inputStream = imageIS) {
			OutputStream out = response.getOutputStream();
			ImageIO.write(image, FilenameUtils.getExtension(url), out);

		} catch (IOException e) {
			// handle
		}
		return new ResponseEntity<Image>(HttpStatus.OK);

	}

	@RequestMapping(path = MENU_ITEM_IMAGE_DELETE_PATH, method = RequestMethod.POST)
	public SMOperationStatus deleteMenuItemtImage(@PathVariable(RootContexts.RESTAURANT_ID) String restaurantId,
			@PathVariable(RootContexts.REFERENCE_IMAGE_ID) String referenceImageId) {
		SMOperationStatus status = new SMOperationStatus();

		try {
			ImageDetail deletedItem = imageDetailRepo.findByReferenceImagePath(referenceImageId);

			if (deletedItem == null) {
				status.setIsSuccessful(false);
				status.setMessage("Requested Image can not found!!!");
				return status;
			}
			String imagePath = deletedItem.getImagePath();

			imageDetailRepo.delete(deletedItem);
			logger.debug("Get url requested for ImageId: {}", deletedItem.getId());

			String url = new String(
					RootContexts.SERVER_IMAGE_PATH + "/" + restaurantId + "/" + "menuitem" + "/" + imagePath);
			File file = new File(url);

			if (file.delete()) {

				status.setMessage(file.getName() + " is deleted!");
				status.setIsSuccessful(true);
			} else {
				System.out.println("Delete operation is failed.");
			}

		} catch (Exception e) {

			e.printStackTrace();

		}

		return status;

	}

	@Consumes("image/*")
	@RequestMapping(path = MENU_ITEM_IMAGE_UPLOAD_PATH, method = RequestMethod.POST)
	SMOperationStatus uploadMenuItemImageHandler(@PathVariable(RootContexts.RESTAURANT_ID) Long restaurantId,
			@PathVariable(RootContexts.MENU_ITEM_ID) Long menuItemId, @RequestParam("image") MultipartFile file,
			@RequestParam(value = "description", required = false) String description,
			@RequestParam(value = "size", required = false) Integer size) {
		SMOperationStatus status = new SMOperationStatus();

		String name = file.getOriginalFilename();

		logger.debug("Get type requested for image: {}", file.getContentType());

		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
				String rootPath = RootContexts.SERVER_IMAGE_PATH + File.separator + restaurantId.toString()
						+ File.separator + "menuitem";

				logger.debug("Get path requested for image: {}", rootPath);
				File dir = new File(rootPath);
				if (!dir.exists()) {

					status.setMessage("Folder doesn't exist. Suggestions: /restaurant, /menu, /menuitem");
					status.setIsSuccessful(false);
					return status;
					// dir.mkdirs();
				}

				// fill database info
				MenuItemImage newMenuItemImage = new MenuItemImage();

				ImageDetail newImageDetail = new ImageDetail();

				MenuItem menuItem = menuItemRepo.findOne(menuItemId);
				if (menuItem == null) {
					status.setRecordId(menuItemId);
					status.setMessage("MenuItem doesn't exist");
					status.setIsSuccessful(false);
					return status;
				}

				File serverFile = new File(dir.getAbsolutePath() + File.separator + name);

				if (serverFile.exists()) {

					name = (new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime())) + name;
					serverFile = new File(dir.getAbsolutePath() + File.separator + name);
				}
				newImageDetail.setImagePath(name);
				newImageDetail.setDescription(description);
				newImageDetail.setSize(size);

				try {
					newImageDetail = imageDetailRepo.saveAndFlush(newImageDetail);
				} catch (Exception e) {
					status.setIsSuccessful(false);
					status.setMessage("Error occured while recording to DB!");
					return status;
				}

				newMenuItemImage.setMenuItem(menuItem);

				try {
					newMenuItemImage = menuItemImageRepo.saveAndFlush(newMenuItemImage);
				} catch (Exception e) {
					status.setIsSuccessful(false);
					status.setMessage("Error occured while recording to DB!");
					return status;
				}

				// Create the file on server
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				serverFile.setReadOnly();// readonly
				stream.close();

				logger.info("Server File Location=" + serverFile.getAbsolutePath());

				status.setMessage("You successfully uploaded file=" + name);
				status.setIsSuccessful(true);
				status.setReferenceId(newImageDetail.getReferenceImagePath());
				status.setRecordId(newImageDetail.getId());
				return status;

			} catch (Exception e) {

				status.setMessage("You failed to upload " + name + " => " + e.getMessage());
				status.setIsSuccessful(false);
				return status;

			}
		} else {
			status.setMessage("You failed to upload " + name + " because the file was empty.");
			status.setIsSuccessful(false);
			return status;

		}
	}

	@RequestMapping(path = MENU_ITEM_UPDATE_IMAGE_INFO_PATH, method = RequestMethod.POST)
	public SMOperationStatus updateMenuItemImageInfo(
			@PathVariable(RootContexts.REFERENCE_IMAGE_ID) String referenceImageId,
			@RequestBody SMMenuItemImage reqToUpdate) {

		SMOperationStatus status = new SMOperationStatus();

		ImageDetail imageToUpdate = imageDetailRepo.findByReferenceImagePath(referenceImageId);
		MenuItemImage menuItem = menuItemImageRepo.findByImageDetail(imageToUpdate);

		if (imageToUpdate == null) {
			status.setIsSuccessful(false);
			status.setMessage("Requested Image can not found!!!");
			return status;
		}

		MenuItem reqItem = menuItemRepo.getOne(reqToUpdate.getMenuItem());
		if (reqItem == null) {
			status.setIsSuccessful(false);
			status.setMessage("Requested Product can not found!!!");
			return status;
		}

		menuItem.setMenuItem(reqItem);
		menuItem = menuItemImageRepo.saveAndFlush(menuItem);

		imageToUpdate.setDescription(reqToUpdate.getDescription());
		imageToUpdate.setSize(reqToUpdate.getSize());

		imageToUpdate = imageDetailRepo.saveAndFlush(imageToUpdate);
		return status;
	}

	// -----------------Menu Image

	@RequestMapping(path = MENU_IMAGE_INFO_PATH, method = RequestMethod.GET)
	public SMMenuImage getMenuImageInfo(@PathVariable(RootContexts.REFERENCE_IMAGE_ID) String referenceImageId) {

		ImageDetail requestImage = new ImageDetail();
		SMMenuImage response = new SMMenuImage();

		requestImage = imageDetailRepo.findByReferenceImagePath(referenceImageId);
		MenuImage menuImage = menuImageRepo.findByImageDetail(requestImage);

		logger.debug("referencePath for Image: {}", requestImage.getReferenceImagePath());

		response.setId(requestImage.getId().toString());
		response.setImagePath(requestImage.getImagePath());
		response.setDescription(requestImage.getDescription());
		response.setSize(requestImage.getSize());
		response.setReferenceImagePath(requestImage.getReferenceImagePath());
		response.setMenu(menuImage.getMenu().getId());
		response.setUpdatedAt(requestImage.getUpdatedAt().toString());

		return response;
	}

	@Produces("image/*")
	@RequestMapping(path = MENU_REFERENCED_IMAGE_PATH, method = RequestMethod.GET)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public HttpEntity getMenuImage(HttpServletResponse response,
			@PathVariable(RootContexts.RESTAURANT_ID) String restaurantId,
			@PathVariable(RootContexts.REFERENCE_IMAGE_ID) String referenceImageId) throws IOException {

		ImageDetail responseItem = imageDetailRepo.findByReferenceImagePath(referenceImageId);
		logger.debug("Get url requested for ImageId: {}", responseItem.getReferenceImagePath());

		String url = new String(
				RootContexts.SERVER_IMAGE_PATH + "/" + restaurantId + "/" + "menu" + "/" + responseItem.getImagePath());
		logger.debug("Get image requested for url: {}", url);

		InputStream imageIS = new BufferedInputStream(new FileInputStream(url));

		BufferedImage image = ImageIO.read(imageIS);

		try (InputStream inputStream = imageIS) {
			OutputStream out = response.getOutputStream();
			ImageIO.write(image, FilenameUtils.getExtension(url), out);

		} catch (IOException e) {
			// handle
		}
		return new ResponseEntity<Image>(HttpStatus.OK);

	}

	@RequestMapping(path = MENU_IMAGE_DELETE_PATH, method = RequestMethod.POST)
	public SMOperationStatus deleteMenuImage(@PathVariable(RootContexts.RESTAURANT_ID) String restaurantId,
			@PathVariable(RootContexts.REFERENCE_IMAGE_ID) String referenceImageId) {
		SMOperationStatus status = new SMOperationStatus();

		try {
			ImageDetail deletedItem = imageDetailRepo.findByReferenceImagePath(referenceImageId);

			if (deletedItem == null) {
				status.setIsSuccessful(false);
				status.setMessage("Requested Image can not found!!!");
				return status;
			}
			String imagePath = deletedItem.getImagePath();

			imageDetailRepo.delete(deletedItem);
			logger.debug("Get url requested for ImageId: {}", deletedItem.getId());

			String url = new String(
					RootContexts.SERVER_IMAGE_PATH + "/" + restaurantId + "/" + "menu" + "/" + imagePath);
			File file = new File(url);

			if (file.delete()) {

				status.setMessage(file.getName() + " is deleted!");
				status.setIsSuccessful(true);
			} else {
				System.out.println("Delete operation is failed.");
			}

		} catch (Exception e) {

			e.printStackTrace();

		}

		return status;

	}

	@Consumes("image/*")
	@RequestMapping(path = MENU_IMAGE_UPLOAD_PATH, method = RequestMethod.POST)
	SMOperationStatus uploadMenuImageHandler(@PathVariable(RootContexts.RESTAURANT_ID) Long restaurantId,
			@PathVariable(RootContexts.MENU_ID) Long menuId, @RequestParam("image") MultipartFile file,
			@RequestParam(value = "description", required = false) String description,
			@RequestParam(value = "size", required = false) Integer size) {
		SMOperationStatus status = new SMOperationStatus();

		String name = file.getOriginalFilename();

		logger.debug("Get type requested for image: {}", file.getContentType());

		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
				String rootPath = RootContexts.SERVER_IMAGE_PATH + File.separator + restaurantId.toString()
						+ File.separator + "menu";

				logger.debug("Get path requested for image: {}", rootPath);
				File dir = new File(rootPath);
				if (!dir.exists()) {

					status.setMessage("Folder doesn't exist. Suggestions: /restaurant, /menu, /menuitem");
					status.setIsSuccessful(false);
					return status;
					// dir.mkdirs();
				}

				// fill database info
				MenuImage newMenuImage = new MenuImage();
				ImageDetail newImageDetail = new ImageDetail();
				Menu menu = menuRepo.findOne(menuId);
				if (menu == null) {
					status.setRecordId(menuId);
					status.setMessage("Menu doesn't exist");
					status.setIsSuccessful(false);
					return status;
				}

				File serverFile = new File(dir.getAbsolutePath() + File.separator + name);

				if (serverFile.exists()) {

					name = (new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime())) + name;
					serverFile = new File(dir.getAbsolutePath() + File.separator + name);
				}
				newImageDetail.setDescription(description);
				newImageDetail.setSize(size);

				try {
					newImageDetail = imageDetailRepo.saveAndFlush(newImageDetail);
				} catch (Exception e) {
					status.setIsSuccessful(false);
					status.setMessage("Error occured while recording to DB!");
					return status;
				}

				newMenuImage.setMenu(menu);

				try {
					newMenuImage = menuImageRepo.saveAndFlush(newMenuImage);
				} catch (Exception e) {
					status.setIsSuccessful(false);
					status.setMessage("Error occured while recording to DB!");
					return status;
				}

				// Create the file on server
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				serverFile.setReadOnly();// readonly
				stream.close();

				logger.info("Server File Location=" + serverFile.getAbsolutePath());

				status.setMessage("You successfully uploaded file=" + name);
				status.setIsSuccessful(true);
				status.setReferenceId(newImageDetail.getReferenceImagePath());
				status.setRecordId(newImageDetail.getId());
				return status;

			} catch (Exception e) {

				status.setMessage("You failed to upload " + name + " => " + e.getMessage());
				status.setIsSuccessful(false);
				return status;

			}
		} else {
			status.setMessage("You failed to upload " + name + " because the file was empty.");
			status.setIsSuccessful(false);
			return status;

		}
	}

	@RequestMapping(path = MENU_UPDATE_IMAGE_INFO_PATH, method = RequestMethod.POST)
	public SMOperationStatus updateMenuImageInfo(@PathVariable(RootContexts.REFERENCE_IMAGE_ID) String referenceImageId,
			@RequestBody SMMenuImage reqToUpdate) {

		SMOperationStatus status = new SMOperationStatus();
		ImageDetail imageToUpdate = imageDetailRepo.findByReferenceImagePath(referenceImageId);
		MenuImage menu = menuImageRepo.findByImageDetail(imageToUpdate);

		if (imageToUpdate == null) {
			status.setIsSuccessful(false);
			status.setMessage("Requested Image can not found!!!");
			return status;
		}

		Menu reqItem = menuRepo.getOne(reqToUpdate.getMenu());
		if (reqItem == null) {
			status.setIsSuccessful(false);
			status.setMessage("Requested Menu can not found!!!");
			return status;
		}

		menu.setMenu(reqItem);
		menu = menuImageRepo.saveAndFlush(menu);

		imageToUpdate.setDescription(reqToUpdate.getDescription());
		imageToUpdate.setSize(reqToUpdate.getSize());

		imageToUpdate = imageDetailRepo.saveAndFlush(imageToUpdate);
		return status;
	}

}
