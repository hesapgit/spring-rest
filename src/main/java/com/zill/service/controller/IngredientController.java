package com.zill.service.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zill.data.dao.IngredientRepository;
import com.zill.data.dao.LanguageRepository;
import com.zill.data.model.Ingredient;
import com.zill.data.model.IngredientTranslation;
import com.zill.service.model.SMIngredient;
import com.zill.service.model.SMIngredientLang;
import com.zill.service.model.SMOperationStatus;
import com.zill.util.RootContexts;

@RestController
@RequestMapping(path = RootContexts.INGREDIENT_ROOT)
@Consumes("application/json; charset=UTF-8")
@Produces("application/json; charset=UTF-8")
public class IngredientController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final String INGREDIENT_ID = "ingredientId";
	private static final String INGREDIENT_PATH_LANGCODE = "/{" + RootContexts.LANGUAGE_CODE + "}";

	private static final String INGREDIENT_PATH_ID = "/{" + INGREDIENT_ID + "}";

	@Autowired
	private IngredientRepository ingredientRepo;

	@Autowired
	private LanguageRepository langRepo;

	// get all ingredients
	@RequestMapping(method = RequestMethod.GET)
	public List<SMIngredient> getAllIngredients() {
		logger.debug("Get all available languages requested for Ingredients");
		List<SMIngredient> responseContent = new ArrayList<>();

		SMIngredient temp;
		for (Ingredient ingre : ingredientRepo.findAll()) {
			temp = new SMIngredient();
			temp.setId(ingre.getId().toString());
			temp.setGenericname(ingre.getGenericName());
			temp.setUpdatedAt(ingre.getLastModifiedDate().toString());
			temp.setTranslations(new ArrayList<>());
			if (ingre.getIngredientTranslations().size() > 0) {
				for (IngredientTranslation ingreTrans : ingre.getIngredientTranslations()) {
					SMIngredientLang ingreLang = new SMIngredientLang();
					ingreLang.setTranslatedname(ingreTrans.getName());
					ingreLang.setLangCode(ingreTrans.getLanguage().getCode());
					temp.getTranslations().add(ingreLang);
				}
			}
			responseContent.add(temp);
		}

		return responseContent;
	}

	@RequestMapping(path = INGREDIENT_PATH_LANGCODE, method = RequestMethod.GET)
	public List<SMIngredient> getIngredientsByLangCode(@PathVariable(RootContexts.LANGUAGE_CODE) String languageCode) {

		logger.debug("Get all available languages requested for Ingredients for Lang Code: {}", languageCode);

		List<SMIngredient> allIngredients = new ArrayList<>();

		SMIngredient temp;
		for (Ingredient ingre : ingredientRepo.findAll()) {

			temp = new SMIngredient();
			temp.setId(ingre.getId().toString());
			temp.setGenericname(ingre.getGenericName());
			temp.setUpdatedAt(ingre.getLastModifiedDate().toString());

			temp.setTranslations(new ArrayList<>());
			if (ingre.getIngredientTranslations().size() > 0) {
				for (IngredientTranslation ingreTrans : ingre.getIngredientTranslations()) {
					if (ingreTrans.getLanguage().getCode().equals(languageCode)) {
						SMIngredientLang ingreLang = new SMIngredientLang();
						ingreLang.setTranslatedname(ingreTrans.getName());
						ingreLang.setLangCode(languageCode);
						ingreLang.setUpdatedAt(ingreTrans.getUpdatedAt().toString());
						temp.getTranslations().add(ingreLang);

					}
				}
			}
			allIngredients.add(temp);
		}
		return allIngredients;
	}

	@RequestMapping(method = RequestMethod.POST)
	public SMOperationStatus addIngredientWithTranslation(@RequestBody SMIngredient reqIngredient) {
		logger.debug("Ingredient name: {}", reqIngredient.getGenericname());

		Ingredient ingredient = new Ingredient();
		ingredient.setGenericName(reqIngredient.getGenericname());
		ingredient.setIngredientTranslations(new ArrayList<>());

		for (SMIngredientLang ingreLanguages : reqIngredient.getTranslations()) {
			IngredientTranslation ingredientTranslation = new IngredientTranslation();
			ingredientTranslation.setName(ingreLanguages.getTranslatedname());
			ingredientTranslation.setLanguage(langRepo.findByCode(ingreLanguages.getLangCode()));
			ingredientTranslation.setIngredient(ingredient);

			ingredient.getIngredientTranslations().add(ingredientTranslation);
		}
		ingredient = ingredientRepo.saveAndFlush(ingredient);

		SMOperationStatus responseItem = new SMOperationStatus();
		responseItem.setRecordId(ingredient.getId());
		responseItem.setIsSuccessful(true);

		return responseItem;
	}

	@RequestMapping(path = INGREDIENT_PATH_ID, method = RequestMethod.POST)
	public SMOperationStatus addIngredientTranslation(@PathVariable(INGREDIENT_ID) Long ingredientId,
			@RequestBody List<SMIngredientLang> reqIngredientLanguages) {

		logger.debug("Add translations to Ingredient id: {}", ingredientId);

		Map<String, SMIngredientLang> mapLangReq = new HashMap<>();
		for (SMIngredientLang ingredientLang : reqIngredientLanguages) {
			mapLangReq.put(ingredientLang.getLangCode(), ingredientLang);

		}

		Ingredient ingredientToUpdateTranslation = ingredientRepo.findOne(ingredientId);

		for (IngredientTranslation ingredientTranslation : ingredientToUpdateTranslation.getIngredientTranslations()) {
			SMIngredientLang currentLang = mapLangReq.get(ingredientTranslation.getLanguage().getCode());

			if (currentLang != null) {
				ingredientTranslation.setName(currentLang.getTranslatedname());
				mapLangReq.remove(currentLang.getLangCode());
			}
		}
		for (SMIngredientLang ingredientLang : mapLangReq.values()) {
			IngredientTranslation ingredientTranslation = new IngredientTranslation();
			ingredientTranslation.setName(ingredientLang.getTranslatedname());
			ingredientTranslation.setLanguage(langRepo.findByCode(ingredientLang.getLangCode()));
			ingredientTranslation.setIngredient(ingredientToUpdateTranslation);

			ingredientToUpdateTranslation.getIngredientTranslations().add(ingredientTranslation);
		}

		ingredientRepo.saveAndFlush(ingredientToUpdateTranslation);

		SMOperationStatus status = new SMOperationStatus();
		status.setIsSuccessful(true);
		status.setRecordId(ingredientToUpdateTranslation.getId());

		return status;
	}

	// update ingredients
	@RequestMapping(path = INGREDIENT_PATH_ID + "/update", method = RequestMethod.POST)
	public SMOperationStatus updateIngredient(@PathVariable(INGREDIENT_ID) Long ingredientId,
			@RequestBody SMIngredient reqIngredient) {
		SMOperationStatus operationStatus = new SMOperationStatus();

		logger.debug("ingredientId id: {}", ingredientId);
		Ingredient ingredientUptoDate = ingredientRepo.getOne(ingredientId);
		if (ingredientUptoDate == null) {
			operationStatus.setIsSuccessful(false);
			operationStatus.setMessage("Requested Ingredient can not be found!!!");
			return operationStatus;
		}

		ingredientUptoDate.setGenericName(reqIngredient.getGenericname());

		Map<String, SMIngredientLang> mapLangTranslation = new HashMap<>();

		for (SMIngredientLang ingredientLang : reqIngredient.getTranslations()) {
			mapLangTranslation.put(ingredientLang.getLangCode(), ingredientLang);
		}

		for (IngredientTranslation ingredientTranslation : ingredientUptoDate.getIngredientTranslations()) {
			SMIngredientLang ingredientLang = mapLangTranslation.get(ingredientTranslation.getLanguage().getCode());
			if (ingredientLang != null) {
				ingredientTranslation.setName(ingredientLang.getTranslatedname());
				mapLangTranslation.remove(ingredientLang.getLangCode());
			}
		}
		for (SMIngredientLang ingredientLang : mapLangTranslation.values()) {
			IngredientTranslation ingredientTranslation = new IngredientTranslation();
			ingredientTranslation.setLanguage(langRepo.getOne(ingredientLang.getLangCode()));
			ingredientTranslation.setName(ingredientLang.getTranslatedname());
			ingredientTranslation.setIngredient(ingredientUptoDate);

			ingredientUptoDate.getIngredientTranslations().add(ingredientTranslation);
		}

		ingredientUptoDate = ingredientRepo.saveAndFlush(ingredientUptoDate);
		operationStatus.setIsSuccessful(true);
		operationStatus.setRecordId(ingredientUptoDate.getId());
		return operationStatus;
	}

	// delete ingredients
	@RequestMapping(path = INGREDIENT_PATH_ID + "/remove", method = RequestMethod.POST)
	public SMOperationStatus deleteIngredient(@PathVariable(INGREDIENT_ID) Long ingredientId) {
		SMOperationStatus operationStatus = new SMOperationStatus();
		ingredientRepo.delete(ingredientId);
		operationStatus.setIsSuccessful(true);
		return operationStatus;
	}
}
