package com.zill.service.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zill.data.dao.ItemsInMenusRepository;
import com.zill.data.dao.LanguageRepository;
import com.zill.data.dao.MenuItemRepository;
import com.zill.data.dao.MenuRepository;
import com.zill.data.dao.RestaurantRepository;
import com.zill.data.model.ItemsInMenus;
import com.zill.data.model.Menu;
import com.zill.data.model.MenuTranslation;
import com.zill.data.model.Restaurant;
import com.zill.service.model.SMItemsInMenu;
import com.zill.service.model.SMMenu;
import com.zill.service.model.SMMenuLang;
import com.zill.service.model.SMOperationStatus;
import com.zill.util.RootContexts;

@RestController
@RequestMapping(path = RootContexts.MENU_ROOT_PATH)
@Consumes("application/json; charset=UTF-8")
@Produces("application/json; charset=UTF-8")
public class MenuController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private MenuRepository menuRepo;

	@Autowired
	private LanguageRepository languageRepo;

	@Autowired
	private RestaurantRepository restaurantRepo;

	@Autowired
	private MenuItemRepository menuItemRepo;

	@Autowired
	private ItemsInMenusRepository itemsInMenuRepo;

	public static final String MENU_ID_ROOT_PATH = "/{" + RootContexts.MENU_ID + "}";

	// get menu with translation and details
	// get menu with items in it
	// createmenu with translation and items
	// createorupdatemenutranslation
	// update items in menu
	// delete menu

	@RequestMapping(path = "/getmenus", method = RequestMethod.GET)
	public List<SMMenu> getAllMenuDetails(@PathVariable(RootContexts.RESTAURANT_ID) Long restaurantId) {

		logger.debug("Get all menus requested for Restaurant: {}", restaurantId);

		List<SMMenu> responseContent = new ArrayList<>();
		List<Menu> menus = menuRepo.findByRestaurantId(restaurantId);
		for (Menu selectedMenu : menus) {

			SMMenu response = new SMMenu();

			response.setGenericname(selectedMenu.getGenericName());
			response.setSpecialOffer(selectedMenu.getSpecialOffer());
			response.setRestaurant(selectedMenu.getRestaurant().getId());
			response.setParentMenuId(selectedMenu.getParentMenuId());
			response.setMenuTranslations(new ArrayList<>());

			for (MenuTranslation menuTranslation : selectedMenu.getMenuTranslations()) {
				SMMenuLang responseMenuLang = new SMMenuLang();
				responseMenuLang.setDescription(menuTranslation.getDescription());
				responseMenuLang.setLangCode(menuTranslation.getLanguage().getCode());
				responseMenuLang.setTitle(menuTranslation.getName());
				response.getMenuTranslations().add(responseMenuLang);
			}

			responseContent.add(response);
		}

		return responseContent;
	}

	@RequestMapping(path = MENU_ID_ROOT_PATH, method = RequestMethod.GET)
	public SMMenu getMenuDetails(@PathVariable(RootContexts.MENU_ID) Long menuId) {

		logger.debug("Get details requested for Menu: {}", menuId);
		Menu selectedMenu = menuRepo.findOne(menuId);

		SMMenu response = new SMMenu();

		response.setGenericname(selectedMenu.getGenericName());
		response.setSpecialOffer(selectedMenu.getSpecialOffer());
		response.setRestaurant(selectedMenu.getRestaurant().getId());
		response.setParentMenuId(selectedMenu.getParentMenuId());
		response.setMenuTranslations(new ArrayList<>());
		response.setItemsInMenus(new ArrayList<>());

		for (MenuTranslation menuTranslation : selectedMenu.getMenuTranslations()) {
			SMMenuLang responseMenuLang = new SMMenuLang();
			responseMenuLang.setDescription(menuTranslation.getDescription());
			responseMenuLang.setLangCode(menuTranslation.getLanguage().getCode());
			responseMenuLang.setTitle(menuTranslation.getName());
			response.getMenuTranslations().add(responseMenuLang);
		}
		for (ItemsInMenus itemInMenu : itemsInMenuRepo.findMenuItemsByMenuId(menuId)) {
			SMItemsInMenu responseItem = new SMItemsInMenu();
			responseItem.setItemDiscountRate(itemInMenu.getItemDiscountRate());
			responseItem.setItemPrice(itemInMenu.getItemPrice());
			responseItem.setMenuItem(itemInMenu.getMenuId());
			response.getItemsInMenus().add(responseItem);

		}

		return response;
	}

	@RequestMapping(method = RequestMethod.POST)
	public SMOperationStatus createMenu(@RequestBody SMMenu menuToAdd,
			@PathVariable(RootContexts.RESTAURANT_ID) Long restaurantId) {

		SMOperationStatus status = new SMOperationStatus();

		Restaurant rest = restaurantRepo.findOne(restaurantId);
		if (rest == null) {

			status.setIsSuccessful(false);
			status.setMessage("Restaurant doesn`t exist");

		}
		Menu newMenu = new Menu();

		newMenu.setGenericName(menuToAdd.getGenericname());
		newMenu.setSpecialOffer(menuToAdd.getSpecialOffer());

		newMenu.setMenuTranslations(new ArrayList<>());

		newMenu.setParentMenuId(menuToAdd.getParentMenuId());

		newMenu.setRestaurant(rest);

		for (SMMenuLang menuLang : menuToAdd.getMenuTranslations()) {
			MenuTranslation mtranslation = new MenuTranslation();

			mtranslation.setDescription(menuLang.getDescription());
			mtranslation.setLanguage(languageRepo.findByCode(menuLang.getLangCode()));
			mtranslation.setName(menuLang.getTitle());
			mtranslation.setMenu(newMenu);

			newMenu.getMenuTranslations().add(mtranslation);
		}

		try {
			newMenu = menuRepo.saveAndFlush(newMenu);

			List<ItemsInMenus> items = new ArrayList<>();

			for (SMItemsInMenu oneItem : menuToAdd.getItemsInMenus()) {
				ItemsInMenus item = new ItemsInMenus();

				item.setItemDiscountRate(oneItem.getItemDiscountRate());
				item.setItemPrice(oneItem.getItemPrice());
				item.setMenuId(newMenu.getId());
				item.setMenuItemId(oneItem.getMenuItem());

				items.add(item);
			}

			if (items.size() > 0) {
				itemsInMenuRepo.save(items);
			}

		} catch (Exception e) {
			status.setIsSuccessful(false);
			status.setMessage("Error occured while recording to DB!");
			return status;
		}

		status.setIsSuccessful(true);
		status.setRecordId(newMenu.getId());

		return status;
	}

	// add translation, update translation, update menu parentId, update menu
	// Specialoffer, menu GenericName
	@RequestMapping(path = MENU_ID_ROOT_PATH + "/updatedetails", method = RequestMethod.POST)
	public SMOperationStatus changeMenuDetails(@RequestBody SMMenu menuToChange,
			@PathVariable(RootContexts.MENU_ID) Long menuId) {

		SMOperationStatus status = new SMOperationStatus();

		Menu selectedMenu = menuRepo.findOne(menuId);
		if (selectedMenu == null) {

			status.setIsSuccessful(false);
			status.setMessage("Menu doesn`t exist");

		}

		selectedMenu.setGenericName(menuToChange.getGenericname());
		selectedMenu.setSpecialOffer(menuToChange.getSpecialOffer());

		selectedMenu.setMenuTranslations(new ArrayList<>());

		selectedMenu.setParentMenuId(menuToChange.getParentMenuId());

		Map<String, SMMenuLang> mapLangTranslation = new HashMap<>();

		for (SMMenuLang menuLang : menuToChange.getMenuTranslations()) {
			mapLangTranslation.put(menuLang.getLangCode(), menuLang);
		}

		for (MenuTranslation menuTranslation : selectedMenu.getMenuTranslations()) {
			SMMenuLang menuLang = mapLangTranslation.get(menuTranslation.getLanguage().getCode());

			if (menuLang != null) {
				menuTranslation.setDescription(menuLang.getDescription());
				menuTranslation.setName(menuLang.getTitle());
				mapLangTranslation.remove(menuLang.getLangCode());
			}
		}

		for (SMMenuLang menuLang : mapLangTranslation.values()) {
			MenuTranslation mTranslation = new MenuTranslation();
			mTranslation.setLanguage(languageRepo.getOne(menuLang.getLangCode()));
			mTranslation.setDescription(menuLang.getDescription());
			mTranslation.setName(menuLang.getTitle());
			mTranslation.setMenu(selectedMenu);

			selectedMenu.getMenuTranslations().add(mTranslation);
		}

		selectedMenu = menuRepo.saveAndFlush(selectedMenu);

		status.setIsSuccessful(true);
		status.setRecordId(selectedMenu.getId());

		return status;
	}

	@RequestMapping(path = MENU_ID_ROOT_PATH + "/updateitems", method = RequestMethod.POST)
	public SMOperationStatus updateItemsInMenu(@PathVariable(RootContexts.MENU_ID) Long menuId,
			@RequestBody SMMenu itemToAdd) {
		SMOperationStatus status = new SMOperationStatus();

		List<ItemsInMenus> selectedItems = itemsInMenuRepo.findMenuItemsByMenuId(menuId);
		ItemsInMenus temp;
		List<ItemsInMenus> itemsInMenus = new ArrayList<>();

		try {
			outerLoop1: for (SMItemsInMenu toAddItems : itemToAdd.getItemsInMenus()) {
				temp = new ItemsInMenus();
				for (ItemsInMenus oneMenuItem : selectedItems) {
					if (toAddItems.getMenuItem() == oneMenuItem.getMenuItemId()) {
						continue outerLoop1;
					}
				}
				temp.setMenuItemId(toAddItems.getMenuItem());
				;
				temp.setMenuId(menuId);
				itemsInMenus.add(temp);
			}
			if (itemsInMenus.size() > 0) {
				itemsInMenuRepo.save(itemsInMenus);
			}

			List<ItemsInMenus> selectedItem2 = itemsInMenuRepo.findMenuItemsByMenuId(menuId);
			outerLoop2: for (ItemsInMenus oneMenuItem : selectedItem2) {
				for (SMItemsInMenu toAddMenuItem : itemToAdd.getItemsInMenus()) {
					if (toAddMenuItem.getMenuItem() == oneMenuItem.getMenuItemId()) {
						continue outerLoop2;
					}
				}
				itemsInMenuRepo.delete(oneMenuItem.getId());
			}

		} catch (Exception e) {
			status.setIsSuccessful(false);
			status.setMessage("Error occured while recording to DB!");
			return status;
		}

		status.setIsSuccessful(true);
		status.setMessage("Changes are saved");
		return status;
	}

	@RequestMapping(path = MENU_ID_ROOT_PATH + "/add/{" + RootContexts.MENU_ITEM_ID + "}", method = RequestMethod.POST)
	public SMOperationStatus addMenuItems(@PathVariable(RootContexts.MENU_ID) Long menuId,
			@PathVariable(RootContexts.MENU_ITEM_ID) Long menuItemId) {
		SMOperationStatus status = new SMOperationStatus();

		List<ItemsInMenus> selectedItemsInMenus = itemsInMenuRepo.findMenuItemsByMenuId(menuId);

		List<ItemsInMenus> items = new ArrayList<>();

		try {

			for (ItemsInMenus oneMenuItem : selectedItemsInMenus) {
				if (menuItemId == oneMenuItem.getMenuItemId()) {
					status.setIsSuccessful(false);
					status.setMessage("Menu Item has already exist!");
					return status;
				}

			}
			ItemsInMenus temp = new ItemsInMenus();
			temp.setMenuItemId(menuItemId);
			temp.setMenuId(menuId);
			items.add(temp);
			if (items.size() > 0) {
				itemsInMenuRepo.save(items);
			}

		} catch (Exception e) {
			status.setIsSuccessful(false);
			status.setMessage("Error occured while recording to DB!");
			return status;
		}

		status.setIsSuccessful(true);

		return status;
	}

	@RequestMapping(path = MENU_ID_ROOT_PATH + "/remove/{" + RootContexts.MENU_ITEM_ID
			+ "}", method = RequestMethod.POST)
	public SMOperationStatus removeMenuIngredients(@PathVariable(RootContexts.MENU_ITEM_ID) Long menuItemId,
			@PathVariable(RootContexts.MENU_ID) Long menuId) {
		SMOperationStatus status = new SMOperationStatus();

		List<ItemsInMenus> selectedItemsInMenus = itemsInMenuRepo.findMenuItemsByMenuId(menuId);

		try {

			for (ItemsInMenus oneMenuItem : selectedItemsInMenus) {
				if (menuItemId == oneMenuItem.getMenuItemId()) {
					itemsInMenuRepo.delete(oneMenuItem);
					status.setIsSuccessful(true);
					status.setMessage("Menu Item removed");
					return status;
				}

			}

		} catch (Exception e) {
			status.setIsSuccessful(false);
			status.setMessage("Error occured while recording to DB!");
			return status;
		}

		status.setIsSuccessful(false);
		status.setMessage("Item does not exist");

		return status;
	}

	@RequestMapping(path = MENU_ID_ROOT_PATH + "/removemenu", method = RequestMethod.POST)
	public SMOperationStatus deleteMenu(@PathVariable(RootContexts.RESTAURANT_ID) Long restaurantId,
			@PathVariable(RootContexts.MENU_ID) Long menuId) {
		SMOperationStatus operationStatus = new SMOperationStatus();
		menuRepo.delete(menuId);
		operationStatus.setIsSuccessful(true);
		return operationStatus;
	}

}
