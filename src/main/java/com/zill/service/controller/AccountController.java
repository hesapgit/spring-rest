package com.zill.service.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zill.data.dao.AccountRepository;
import com.zill.data.dao.AccountStatusRepository;
import com.zill.data.dao.AuthenticationProviderRepository;
import com.zill.data.dao.CurrencyRepository;
import com.zill.data.dao.LanguageRepository;
import com.zill.data.dao.MenuGroupRepository;
import com.zill.data.dao.MenuItemIngredientRepository;
import com.zill.data.dao.MenuItemRepository;
import com.zill.data.dao.MenuItemTranslationRepository;
import com.zill.data.dao.MenuRepository;
import com.zill.data.dao.ProfileRepository;
import com.zill.data.dao.RestaurantRepository;
import com.zill.data.dao.RoleRepository;
import com.zill.data.dao.UserMenuRepository;
import com.zill.data.dao.UserRestaurantRepository;
import com.zill.data.model.Account;
import com.zill.data.model.AccountStatus;
import com.zill.data.model.Menu;
import com.zill.data.model.MenuItem;
import com.zill.data.model.MenuItemIngredient;
import com.zill.data.model.MenuItemTranslation;
import com.zill.data.model.Profile;
import com.zill.data.model.Restaurant;
import com.zill.data.model.UserMenu;
import com.zill.data.model.UserRestaurant;
import com.zill.service.model.SMAccount;
import com.zill.service.model.SMAccountStatus;
import com.zill.service.model.SMMenuProduct;
import com.zill.service.model.SMMenuProductLang;
import com.zill.service.model.SMOperationStatus;
import com.zill.service.model.SMProfile;
import com.zill.util.RootContexts;

@RestController
@RequestMapping(path = RootContexts.USER_ROOT_PATH)
@Consumes("application/json; charset=UTF-8")
@Produces("application/json; charset=UTF-8")
public class AccountController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private AccountRepository accountRepo;

	@Autowired
	private UserMenuRepository userMenuRepo;
	@Autowired
	private UserRestaurantRepository userRestaurantRepo;
	@Autowired
	private AccountStatusRepository accountStatusRepo;

	@Autowired
	private AuthenticationProviderRepository authenticationProviderRepo;

	@Autowired
	private RoleRepository roleRepo;

	@Autowired
	private MenuItemRepository menuItemRepo;

	@Autowired
	private ProfileRepository profileRepo;

	@Autowired
	private RestaurantRepository restaurantRepo;
	
	@Autowired
	private MenuRepository menuRepo;

	// createAccount
	public static final String CREATE_ACCOUNT_PATH = "/create";
	// updateAccount
	public static final String UPDATE_ACCOUNT_PATH = "/updateaccount/{" + RootContexts.USER_ID + "}";
	// UpdateProfile
	public static final String UPDATE_PROFILE_PATH = "/updateaccount/{" + RootContexts.PROFILE_ID + "}";
	// getAccountDetail
	public static final String DETAIL_ACCOUNT_PATH = "/account/{" + RootContexts.USER_ID + "}";
	//addUserMenuList
	public static final String ADD_MENU_USER_PATH = "/update/{" + RootContexts.USER_ID+ "}/addmenu/{"+ RootContexts.MENU_ID + "}";
	//removeUserMenuList
	public static final String REMOVE_MENU_USER_PATH = "/update/{" + RootContexts.USER_ID+ "}/removemenu/{"+ RootContexts.MENU_ID + "}";
	//addUserRestaurantList
	public static final String ADD_RESTAURANT_USER_PATH = "/update/{" + RootContexts.USER_ID+ "}/addrestaurant/{"+ RootContexts.RESTAURANT_ID + "}";
	//removeUserRestaurantList
	public static final String REMOVE_RESTAURANT_USER_PATH = "/update/{" + RootContexts.USER_ID+ "}/removerestaurant/{"+ RootContexts.RESTAURANT_ID + "}";
	// ListStatus
	public static final String LIST_STATUS_PATH = "/liststatus";
	//ListAccount
	public static final String LIST_ACCOUNTS = "/listaccounts";
	
	// create account 
	
	@RequestMapping(path = CREATE_ACCOUNT_PATH, method = RequestMethod.POST)
	public SMOperationStatus createAccount(@RequestBody SMAccount newUser) {
		SMOperationStatus status = new SMOperationStatus();

		Account newAccount = new Account();
		Profile newProfile = new Profile();

	

		try {
			newProfile.setFirstName(newUser.getProfile().getFirstName());
			newProfile.setFullName(newUser.getProfile().getFullName());
			newProfile.setLastName(newUser.getProfile().getLastName());
			newProfile.setEmail(newUser.getEmail());
			newProfile = profileRepo.saveAndFlush(newProfile);
		} catch (Exception e) {
			status.setIsSuccessful(false);
			status.setMessage("Error occured while New Profile to DB!");
			return status;
		}
		

		newAccount.setPassword(BCrypt.hashpw(newUser.getPassword(), BCrypt.gensalt(12)));
		newAccount.setUsername(newUser.getUsername());
		newAccount.setAccountStatus(accountStatusRepo.findOne(newUser.getAccountStatus().getId()));
		
		newAccount.setProfile(newProfile);

		try {
			newAccount = accountRepo.saveAndFlush(newAccount);

			List<UserMenu> userMenus = new ArrayList<>();

			for (Long menuId : newUser.getUserMenu()) {
				UserMenu userMenu = new UserMenu();
				userMenu.setProfile(newProfile.getId());
				userMenu.setMenu(menuId);
				userMenus.add(userMenu);
			}

			if (userMenus.size() > 0) {
				userMenuRepo.save(userMenus);
			}

			List<UserRestaurant> userRestaurants = new ArrayList<>();

			for (Long restaurantId : newUser.getUserRestaurant()) {
				UserRestaurant userRestaurant = new UserRestaurant();
				userRestaurant.setProfile(newProfile.getId());
				userRestaurant.setRestaurant(restaurantId);
				userRestaurants.add(userRestaurant);
			}

			if (userRestaurants.size() > 0) {
				userRestaurantRepo.save(userRestaurants);
			}

		} catch (Exception e) {
			status.setIsSuccessful(false);
			status.setMessage("Error occured while setting Responsibilies to DB!");
			return status;
		}

		status.setIsSuccessful(true);
		return status;
	}
	
	// update profile 
	
	@RequestMapping(path = UPDATE_PROFILE_PATH, method = RequestMethod.POST)	
	 public SMOperationStatus updateProfile(@RequestBody SMProfile
			 userToUpdate,@PathVariable(RootContexts.PROFILE_ID) Long profileId){
			 SMOperationStatus status = new SMOperationStatus();
			
			 
			 Profile selectedProfile = profileRepo.findOne(profileId);
			
			 
			
			
			 selectedProfile.setFirstName(userToUpdate.getFirstName());
			 selectedProfile.setFullName(userToUpdate.getFullName());
			 selectedProfile.setLastName(userToUpdate.getLastName());
			 
			 try {
			 selectedProfile=profileRepo.saveAndFlush(selectedProfile);
			 
			 } catch (Exception e) {
			 status.setIsSuccessful(false);
			 status.setMessage("Error occured while New Profile to DB!");
			 return status;
			 }
			
			
		
			
			 try {		 
			
			 List<UserMenu> userMenus = new ArrayList<>();
			
			 for (Long menuId : userToUpdate.getMenuId()) {
			 UserMenu userMenu = new UserMenu();
			 userMenu.setProfile(selectedProfile.getId());
			 userMenu.setMenu(menuId);
			 userMenus.add(userMenu);
			 }
			
			 if (userMenus.size() > 0) {
			 userMenuRepo.save(userMenus);
			 }
			
			 List<UserRestaurant> userRestaurants = new ArrayList<>();
			
			 for (Long restaurantId : userToUpdate.getRestaurantId()) {
			 UserRestaurant userRestaurant = new UserRestaurant();
			 userRestaurant.setProfile(selectedProfile.getId());
			 userRestaurant.setRestaurant(restaurantId);
			 userRestaurants.add(userRestaurant);
			 }
			
			 if (userRestaurants.size() > 0) {
			 userRestaurantRepo.save(userRestaurants);
			 }
			
			 } catch (Exception e) {
			 status.setIsSuccessful(false);
			 status.setMessage("Error occured while setting Responsibilies to DB!");
			 return status;
			 }
			
			 status.setIsSuccessful(true);
			 return status;
			
			 }

	//
	 @RequestMapping(path = DETAIL_ACCOUNT_PATH, method = RequestMethod.GET)
	 public SMAccount getDetailAccount(@PathVariable(RootContexts.USER_ID) Long userId){
	 
	
     SMAccount responseAccount = new SMAccount();
	 Account selectedAccount = accountRepo.findOne(userId);
	 
	 SMProfile responseProfile = new SMProfile();
	 SMAccountStatus responseAccountStatus = new SMAccountStatus();
	 
	 
	 responseAccount.setCreatedAt(selectedAccount.getCreatedAt().toString());
	 responseAccount.setUpdatedAt(selectedAccount.getUpdatedAt().toString());
	 responseAccount.setRole(selectedAccount.getId().toString());
	 responseAccount.setEmail(selectedAccount.getProfile().getEmail());
	 responseAccount.setUsername(selectedAccount.getUsername());
	 responseAccount.setUserMenu(new ArrayList<>());
	 responseAccount.setUserRestaurant(new ArrayList<>());
	 
	 responseProfile.setFirstName(selectedAccount.getProfile().getFirstName());
	 responseProfile.setFullName(selectedAccount.getProfile().getFullName());
	 responseProfile.setLastName(selectedAccount.getProfile().getLastName());
	 responseProfile.setId(userId.toString());
	 responseAccount.setProfile(responseProfile);
	 
	 responseAccountStatus.setCode(selectedAccount.getAccountStatus().getCode());
	 responseAccountStatus.setName(selectedAccount.getAccountStatus().getName());	 
	 responseAccount.setAccountStatus(responseAccountStatus);
	 
	 for(UserMenu menu : userMenuRepo.findByProfileId(selectedAccount.getProfile().getId())){
		 
		 responseAccount.getUserMenu().add(menu.getId());
		 
	 }
	 
 for(UserRestaurant restaurant : userRestaurantRepo.findByProfileId(selectedAccount.getProfile().getId())){
		 
		 responseAccount.getUserRestaurant().add(restaurant.getId());
		 
	 }
	
	return responseAccount;
	 }
	 
	 
	 
	 @RequestMapping(path = LIST_ACCOUNTS, method = RequestMethod.GET)
	 public List<SMAccount> listAccounts(){
	 List<SMAccount> responseContent = new ArrayList<>();
	
     SMAccount responseAccount = new SMAccount();
	 List<Account> allAccount = accountRepo.findAll();
	 
	 for(Account selectedAccount : allAccount){
		 SMProfile responseProfile = new SMProfile();
		 SMAccountStatus responseAccountStatus = new SMAccountStatus();
		 
		 
		 responseAccount.setCreatedAt(selectedAccount.getCreatedAt().toString());
		 responseAccount.setUpdatedAt(selectedAccount.getUpdatedAt().toString());
		 responseAccount.setRole(selectedAccount.getId().toString());
		 responseAccount.setEmail(selectedAccount.getProfile().getEmail());
		 responseAccount.setUsername(selectedAccount.getUsername());
		 responseAccount.setUserMenu(new ArrayList<>());
		 responseAccount.setUserRestaurant(new ArrayList<>());
		 
		 responseProfile.setFirstName(selectedAccount.getProfile().getFirstName());
		 responseProfile.setFullName(selectedAccount.getProfile().getFullName());
		 responseProfile.setLastName(selectedAccount.getProfile().getLastName());
		 responseProfile.setId(selectedAccount.getProfile().getId().toString());
		 responseAccount.setProfile(responseProfile);
		 
		 responseAccountStatus.setCode(selectedAccount.getAccountStatus().getCode());
		 responseAccountStatus.setName(selectedAccount.getAccountStatus().getName());	 
		 responseAccount.setAccountStatus(responseAccountStatus);
		 
		 for(UserMenu menu : userMenuRepo.findByProfileId(selectedAccount.getProfile().getId())){
			 
			 responseAccount.getUserMenu().add(menu.getId());
			 
		 }
		 
	 for(UserRestaurant restaurant : userRestaurantRepo.findByProfileId(selectedAccount.getProfile().getId())){
			 
			 responseAccount.getUserRestaurant().add(restaurant.getId());
			 
		 }
	 
	 responseContent.add(responseAccount);
	 }
	
	
	 	return responseContent;
	 }
	 
	 @RequestMapping(path = LIST_STATUS_PATH, method = RequestMethod.GET)
	 public List<SMAccountStatus> listAccoutStatus(){
		 
		 List<AccountStatus> allAccountStatus = accountStatusRepo.findAll();
		 
		 List<SMAccountStatus> responseContent = new ArrayList<>(); 
		 
		 for(AccountStatus oneStatus : allAccountStatus){
			 
			 SMAccountStatus response = new SMAccountStatus();
			 
			 response.setCode(oneStatus.getCode());
			 response.setName(oneStatus.getName());
			 response.setId(oneStatus.getId());
			 responseContent.add(response);
		 }
		 
		 
		 return responseContent;
	 }
	 
	 @RequestMapping(path = ADD_MENU_USER_PATH, method = RequestMethod.POST)	
	 public SMOperationStatus addUserMenu(@PathVariable(RootContexts.MENU_ID) Long menuId,@PathVariable(RootContexts.USER_ID) Long userId){
		 SMOperationStatus status = new SMOperationStatus();
		 
		Account selectedAccount = accountRepo.findOne(userId);
		Menu selectedMenu = menuRepo.findOne(menuId);
		
		 
		// check new menu whether it belongs to restaurant.
		 for(UserRestaurant userRestaurant :userRestaurantRepo.findByProfileId(selectedAccount.getProfile().getId()) ){
			 if(userRestaurant.getRestaurant()==selectedMenu.getRestaurant().getId()){
				 	 continue;
			 }
			 
			 status.setMessage("Error occured while recording to DB!!");
			 status.setIsSuccessful(false);
				return status; 
		 	}
		
		 
		 List<UserMenu> selectedUserMenus = userMenuRepo.findByProfileId(selectedAccount.getProfile().getId());
		 List<UserMenu> menus = new ArrayList<>();

			try {

				for (UserMenu oneSelectedMenu : selectedUserMenus) {
					if (menuId == oneSelectedMenu.getMenu()) {
						status.setIsSuccessful(false);
						status.setMessage("Ingredient has already exist!");
						return status;
					}

				}
				UserMenu temp = new UserMenu();
				temp.setMenu(menuId);
				temp.setProfile(selectedAccount.getProfile().getId());
				menus.add(temp);
				if (menus.size() > 0) {
					userMenuRepo.save(menus);
				}

			} catch (Exception e) {
				status.setIsSuccessful(false);
				status.setMessage("Error occured while recording to DB!");
				return status;
			}
		 
			status.setIsSuccessful(true);
			return status;
			
			 } 
	 
	 @RequestMapping(path = REMOVE_RESTAURANT_USER_PATH, method = RequestMethod.POST)	
	 public SMOperationStatus removeUserRestaurant(@PathVariable(RootContexts.RESTAURANT_ID) Long restaurantId,@PathVariable(RootContexts.USER_ID) Long userId){
		 SMOperationStatus status = new SMOperationStatus();
		 
		 Account selectedAccount = accountRepo.findOne(userId);
		 List<UserRestaurant> selectedRestaurants = userRestaurantRepo.findByProfileId(selectedAccount.getProfile().getId());
		 List<UserMenu> selectedMenus = userMenuRepo.findByProfileId(selectedAccount.getProfile().getId());
			try {
				// delete restaurant
				for (UserRestaurant oneSelectedeMenu : selectedRestaurants) {
					if (restaurantId == oneSelectedeMenu.getRestaurant()) {
						userRestaurantRepo.delete(oneSelectedeMenu);
					}

				}
				
				// delete also menus belonged to restaurant
				for (UserMenu oneSelectedMenu : selectedMenus) {
					if (restaurantId == menuRepo.findOne(oneSelectedMenu.getMenu()).getRestaurant().getId()) {
						userMenuRepo.delete(oneSelectedMenu);
					}

				}

			} catch (Exception e) {
				status.setIsSuccessful(false);
				status.setMessage("Error occured while recording to DB!");
				return status;
			}

			status.setIsSuccessful(true);
			status.setMessage("Restaurant deleted from user");
			return status;
	 }
	 
	 @RequestMapping(path = ADD_RESTAURANT_USER_PATH, method = RequestMethod.POST)	
	 public SMOperationStatus addUserRestauant(@PathVariable(RootContexts.RESTAURANT_ID) Long restaurantId,@PathVariable(RootContexts.USER_ID) Long userId){
		 SMOperationStatus status = new SMOperationStatus();
		 
		Account selectedAccount = accountRepo.findOne(userId);
		
		 
		 List<UserRestaurant> selectedUserRestaurants = userRestaurantRepo.findByProfileId(selectedAccount.getProfile().getId());
		 List<UserRestaurant> restaurants = new ArrayList<>();

			try {

				for (UserRestaurant oneSelectedRestaurant : selectedUserRestaurants) {
					if (restaurantId == oneSelectedRestaurant.getRestaurant()) {
						status.setIsSuccessful(false);
						status.setMessage("Ingredient has already exist!");
						return status;
					}

				}
				UserRestaurant temp = new UserRestaurant();
				temp.setRestaurant(restaurantId);
				temp.setProfile(selectedAccount.getProfile().getId());
				restaurants.add(temp);
				if (restaurants.size() > 0) {
					userRestaurantRepo.save(restaurants);
				}

			} catch (Exception e) {
				status.setIsSuccessful(false);
				status.setMessage("Error occured while recording to DB!");
				return status;
			}
		 
			status.setIsSuccessful(true);
			return status;
			
			 } 
	 
	 @RequestMapping(path = REMOVE_MENU_USER_PATH, method = RequestMethod.POST)	
	 public SMOperationStatus removeUserMenu(@PathVariable(RootContexts.MENU_ID) Long menuId,@PathVariable(RootContexts.USER_ID) Long userId){
		 SMOperationStatus status = new SMOperationStatus();
		 
		 Account selectedAccount = accountRepo.findOne(userId);
		 List<UserMenu> selectedMenus = userMenuRepo.findByProfileId(selectedAccount.getProfile().getId());

			try {

				for (UserMenu oneSelectedeMenu : selectedMenus) {
					if (menuId == oneSelectedeMenu.getMenu()) {
						userMenuRepo.delete(oneSelectedeMenu);
						status.setIsSuccessful(true);
						status.setMessage("Ingredient deleted");
						return status;
					}

				}

			} catch (Exception e) {
				status.setIsSuccessful(false);
				status.setMessage("Error occured while recording to DB!");
				return status;
			}

			status.setIsSuccessful(false);
			status.setMessage("Ingredient not exist");

			return status;
	 }
}
